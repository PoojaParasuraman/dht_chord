#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <getopt.h>
#include <unistd.h>
#include <arpa/inet.h>

#define P2P_LITTLE_ENDIAN  1
#define P2P_DHT_REPLICATION 2
#define P2P_DHT_TTL        10

int fd                    = -1;
struct in_addr addr       = {0};
uint16_t port             = 0;
uint8_t key_array[10][32] = {0};
int count                 = 0;

void handle_put_request();
int send_put_request(uint8_t *key, uint8_t *value, uint8_t length);

/******************* SOCK HELPER ************************************************/
static inline void Get1B(uint8_t **packet, uint8_t *dest)
{
    *dest = **packet; (*packet)++;
}

static inline void Put1B(uint8_t *dest, uint8_t **packet)
{
    **packet = *dest; (*packet)++;
}

static inline void Get2B(uint8_t **packet, uint16_t *dest)
{
#if P2P_BIG_ENDIAN == 1
    *(((uint8_t *)dest)+0) = **packet; (*packet)++;
    *(((uint8_t *)dest)+1) = **packet; (*packet)++;
#elif P2P_LITTLE_ENDIAN == 1
    *(((uint8_t *)dest)+1) = **packet; (*packet)++;
    *(((uint8_t *)dest)+0) = **packet; (*packet)++;
#endif
}

static inline void Put2B(uint16_t *dest, uint8_t **packet)
{
#if P2P_BIG_ENDIAN == 1
    **packet = *(((uint8_t *)dest)+0); (*packet)++;
    **packet = *(((uint8_t *)dest)+1); (*packet)++;
#elif P2P_LITTLE_ENDIAN == 1
    **packet = *(((uint8_t *)dest)+1); (*packet)++;
    **packet = *(((uint8_t *)dest)+0); (*packet)++;
#endif
}

static inline void GetnB(uint8_t **packet, void *dest, uint32_t n)
{
    memcpy(dest, *packet, n);
    (*packet) += n;
}
static inline void PutnB(void *dest, uint8_t **packet, uint32_t n)
{
    memcpy(*packet, dest, n);
    (*packet) += n;
}

/*************************************************************************************/

int p2p_socket_connect(int sockfd) {
    struct sockaddr_in remote = {0};
    int status                = -1;

    do {
        if (-1 == sockfd) {
            printf("%s: Invalid socket fd",__func__);
            break;
        }

        printf("Attempting to connect!\n");
        bzero(&remote, sizeof(remote));
        memcpy(&remote.sin_addr, &addr, sizeof(struct in_addr));
        remote.sin_family = AF_INET;
        remote.sin_port   = htons(port);
        status = connect(sockfd , (struct sockaddr *)&remote , sizeof(struct sockaddr));

        printf("\nConnected! fd - %d\n ", sockfd);
    } while (0);

    return status;
}

int test_client_init() {
    int status        = 0;

    do {
        /* Create socket */
        fd = socket(AF_INET, SOCK_STREAM, 0);
	if(-1 == fd) {
            printf("%s: Could not create socket",__func__);
            status = -1;
            break;
        }

        /* Connect to remote server */
        if (p2p_socket_connect(fd) < 0) {
             printf("%s: Failed connecting to server",__func__);
             status = -1;
             break;
        }

    } while (0);

    return status;
}

static void dht_print_usage()
{
    printf("\n ----------------------- DHT ------------------------");
    printf("\n -a   option to provide IPV4 address of api node");
    printf("\n -p   option to povide Port Number of API Node");
    printf("\n ----------------------------------------------------\n");
}

int send_get_request(uint8_t *key, uint8_t **value) {
    uint8_t *msg,*p       = NULL;
    uint8_t  buffer[6500] = {0};
    uint16_t len          = 0;
    int size              = 0;
    int length            = 0;
    uint16_t type         = 0;

    if (-1 == fd) {
        printf("\n Invalid Socket fd");
        return -1;
    }

    len  = 36;
    type = 651;

    msg = p = (uint8_t *) calloc (1, sizeof(uint8_t) *len);
    if (!msg) {
        printf("%s : Calloc failure for msg",__func__);
        return -1;
    }

    Put2B(&len, &p);
    Put2B(&type, &p);
    PutnB(key, &p, 32);

    if (-1 == (size = send(fd, msg, len, 0))) {
        printf("%s: send failed", __func__);
        free(msg);
        return -1;
    }

    free(msg);

    if (len != size) {
        printf("%s: Mismatch of bytes sent : sent - %d, actual - %d",__func__,size,len);
        return -1;
    }

    length = read(fd, buffer, 6500);

    if (-1 == length) {
        printf("\n%s Could not read data from socket\n",__func__);
        return -1;
    }

    *value = (uint8_t *) calloc (1, sizeof(uint8_t) * length);

    if (!(*value)) {
	printf("\n Calloc failure");
        return -1;
    }

    memcpy (*value, buffer, length);
    return 0;
}

void handle_get_request() {
    uint16_t size   = 0;
    uint16_t type   = 0;
    uint8_t  *buf   = NULL;
    uint8_t  data[6500] = {0};
    uint8_t key[32] = {0};
    int i           = 0;
    int j           = 0;
    int choice      = -1;

    printf("\n------------- Choose which key to retrieve ------------------\n");
    for (i = 0; i < count; i++) {
	printf("\n(%d)\t",i);
        for (j = 0; j < 32; j++) {
	    printf("%02hhx ",key_array[i][j]);
	}
    }
    printf("\n(%d)\t",i);
    printf("Any other randomly generated key\n");

    printf("\n Your choice:\t");
    scanf("%d",&choice);

    if ((choice >= 0) && (choice < count)) {
        memcpy(key, key_array[choice], 32);
    }
    else if (choice == i) {
        for(i = 0; i < 32; i++) {
            key[i] = (uint8_t) rand();
        }
    }
    else {
        printf("\n Invalid choice\n");
	return;
    }

    printf("\n------------------- requesting for Key ----------------------\n");
    for(i = 0; i < 32; i++) {
        printf("%02hhx ",key[i]);
    }
    printf("\n");


    if (-1 == send_get_request(key, &buf)) {
        printf("\n-------------------  Get Request Failed ----------------------");
        return;
    }

    if (!buf) {
        printf("\n NULL Buffer\n");
        return;
    }

    Get2B(&buf, &size);
    Get2B(&buf, &type);

    if (type == 653) {
        printf("\n-------------------  Received DHT Failure  ----------------------");
    }
    else if (type == 652) {

        printf("\n-------------------  Received DHT Success ----------------------");

	if (size == 4) {
            printf("\n Size of Data is zero :(");
	}
	else {
            GetnB(&buf, key, 32);
            GetnB(&buf, data, size-36);

            printf("\n Retrieved data with length %d : \n",size-36);
            for(i = 0; i < size-36; i++) {
                printf("%02hhx ",data[i]);
            }
            printf("\n");
	}
    }
    else {
        printf("\n-------------------  Received Invalid msg  ----------------------");
    }
    printf("\n------------------------------------------------------------------");

    return;
}

int send_put_request(uint8_t *key, uint8_t *value, uint8_t length) {
    uint8_t *msg,*p     = NULL;
    uint16_t len        = 0;
    int size            = 0;
    uint16_t type       = 0;
    uint16_t ttl        = 0;
    uint8_t replication = 0;
    uint8_t reserved    = 0;

    if (-1 == fd) {
        printf("\n Invalid Socket fd");
        return -1;
    }

    len         = 40 + length;
    type        = 650;
    ttl         = P2P_DHT_TTL;
    replication = P2P_DHT_REPLICATION;
    reserved    = 0;

    msg = p = (uint8_t *) calloc (1, sizeof(uint8_t) *len);
    if (!msg) {
        printf("%s : Calloc failure for msg",__func__);
        return -1;
    }

    Put2B(&len, &p);
    Put2B(&type, &p);
    Put2B(&ttl, &p);
    Put1B(&replication, &p);
    Put1B(&reserved, &p);
    PutnB(key, &p, 32);
    PutnB(value, &p, length);
    if (-1 == (size = send(fd, msg, len, 0))) {
        printf("%s: send failed", __func__);
	free(msg);
        return -1;
    }

    free(msg);

    if (len != size) {
        printf("%s: Mismatch of bytes sent : sent - %d, actual - %d",__func__,size,len);
        return -1;
    }

    return 0;
}

void handle_put_request() {
    uint8_t key[32] = {0};
    int i           = 0;

    for(i = 0; i < 32; i++) {
        key[i] = (uint8_t) rand();
    }

    uint8_t len   = (uint8_t) rand();
    uint8_t *data = (uint8_t *) calloc (1, sizeof(uint8_t) * len);

    for(i = 0; i < len; i++) {
        data[i] = (uint8_t) rand();
    }

    if (-1 == send_put_request(key, data, len)) {
        printf("\n-------------------  Put Request Failed ----------------------");
        return;
    }

    printf("\n-------------------  Put Request Successful ----------------------");
    printf("\n Generated Key : \n");
    for(i = 0; i < 32; i++) {
        printf("%02hhx ",key[i]);
    }
    printf("\n");

    printf("\n Generated data with length %d : \n",len);
    for(i = 0; i < len; i++) {
        printf("%02hhx ",data[i]);
    }
    printf("\n");
    printf("\n------------------------------------------------------------------");
    memcpy(key_array[count], key, 32);
    count++;

    return;
}

int main(int argc, char *argv[]) {
    int opt          = 0;
    int task         = 0;
    int addr_found   = 0;
    int port_found   = 0;
    const char *adr  = NULL;

    while ((opt = getopt (argc, argv, "a:p:"))!= -1)
    {
        switch (opt)
        {
            case 'a':
                adr        = optarg;
                addr_found = 1;
                break;
            case 'p':
                port       = atoi(optarg);
                port_found = 1;
                break;
            case '?':
            default:
                dht_print_usage();
                return -1;
        }
    }
    if (!(port_found && addr_found)) {
        printf("\n Insufficient arguments");
        dht_print_usage();
        return -1;
    }

    if (1 != inet_pton(AF_INET, adr, &addr)) {
        printf("%s: Invalid IPv4 address",__func__);
        return -1;
    }
 
    if (-1 == test_client_init()) {
        printf("%s: Failed to connect to DHT",__func__);
        return -1;
    }

    while (1) {
        printf("\n What do you wanna do? Type");
	printf("\n     1 - Put");
	printf("\n     2 - Get");
	printf("\n     3 - Quit");
	printf("\n Your Answer: ");
	scanf("%d", &task);

	switch(task) {
	    case 2: {
		handle_get_request();
		break;
	    }
            case 1: {
		handle_put_request();
	        break;
	    }
            case 3: {
	        close(fd);
	        printf( "\n Have a nice day Bye!\n");
		return 0;
            }
	    default: {
	        printf("\n Invalid Input\n");
		break;
            }
	}
    }

    return 0;
}
