#include <unordered_map>
#include <iostream>
#include <dht_hashmap.h>

using namespace std;

class Hashmap {
    private:
        unordered_map<dht_key_t, value_t> umap;

    public:

        uint8_t lookup(dht_key_t key, value_t *value) {
            //cout << "lookup key:" << std::hex << key << "\n";
            /* check for entry */
            if (umap.find(key) == umap.end()) {
                return 0;
            }
            *value = umap[key];
            //cout << "lookup value:" << value << "\n";
            return 1;
        }
        void update(dht_key_t key, value_t value) {
            //cout << "insert key:" << std::hex << key << " value:" << value << "\n";
            /* check if map entry already available */
            if (umap.find(key) == umap.end()) {
                umap.insert(make_pair(key, value));
            } else {
                umap[key] = value;
            }
        }
        void erase (dht_key_t key) {
            umap.erase(key);
        }
        void reset() {
            umap.clear();
        }

	int8_t iterate(void callback(dht_key_t, value_t, dht_key_t), dht_key_t key) {
            for ( auto it = umap.begin(); it != umap.end(); ++it )
		    callback(it->first, it->second, key);
	    return 0;
	}
};
