#ifndef DHT_UTILS_H
#define DHT_UTILS_H

#include <time.h>
#include <string.h>
#include <stdint.h>

#include "dht_hashmap.h"

#define MAX_KEY_STR_LEN 20
#define P2P_LITTLE_ENDIAN 1

struct timespec get_current_time();
uint64_t get_clock_diff_secs(struct timespec new_time, struct timespec old_time);
uint64_t dht_get_hash_key (uint8_t * key);
int dht_sendto_thread (void *data, int type, int fd);
uint64_t dht_get_node_key (uint16_t port, uint32_t ipv4);
int compare_handle_entry (void *node_data, void *data);

static inline void Get1B(uint8_t **packet, uint8_t *dest)
{
    *dest = **packet; (*packet)++;
}

static inline void Put1B(uint8_t *dest, uint8_t **packet)
{
    **packet = *dest; (*packet)++;
}

static inline void Get2B(uint8_t **packet, uint16_t *dest)
{
#if P2P_BIG_ENDIAN == 1
    *(((uint8_t *)dest)+0) = **packet; (*packet)++;
    *(((uint8_t *)dest)+1) = **packet; (*packet)++;
#elif P2P_LITTLE_ENDIAN == 1
    *(((uint8_t *)dest)+1) = **packet; (*packet)++;
    *(((uint8_t *)dest)+0) = **packet; (*packet)++;
#endif
}

static inline void Put2B(uint16_t *dest, uint8_t **packet)
{
#if P2P_BIG_ENDIAN == 1
    **packet = *(((uint8_t *)dest)+0); (*packet)++;
    **packet = *(((uint8_t *)dest)+1); (*packet)++;
#elif P2P_LITTLE_ENDIAN == 1
    **packet = *(((uint8_t *)dest)+1); (*packet)++;
    **packet = *(((uint8_t *)dest)+0); (*packet)++;
#endif
}

static inline void Get4B(uint8_t **packet, uint32_t *dest)
{
#if P2P_BIG_ENDIAN == 1
    *(((uint8_t *)dest)+0)  = **packet; (*packet)++;
    *(((uint8_t *)dest)+1)  = **packet; (*packet)++;
    *(((uint8_t *)dest)+2)  = **packet; (*packet)++;
    *(((uint8_t *)dest)+3)  = **packet; (*packet)++;
#elif P2P_LITTLE_ENDIAN == 1
    *(((uint8_t *)dest)+3)  = **packet; (*packet)++;
    *(((uint8_t *)dest)+2)  = **packet; (*packet)++;
    *(((uint8_t *)dest)+1)  = **packet; (*packet)++;
    *(((uint8_t *)dest)+0)  = **packet; (*packet)++;
#endif
}

static inline void Put4B(uint32_t *dest, uint8_t **packet)
{
#if P2P_BIG_ENDIAN == 1
    **packet = *(((uint8_t *)dest)+0); (*packet)++;
    **packet = *(((uint8_t *)dest)+1); (*packet)++;
    **packet = *(((uint8_t *)dest)+2); (*packet)++;
    **packet = *(((uint8_t *)dest)+3); (*packet)++;
#elif P2P_LITTLE_ENDIAN == 1
    **packet = *(((uint8_t *)dest)+3); (*packet)++;
    **packet = *(((uint8_t *)dest)+2); (*packet)++;
    **packet = *(((uint8_t *)dest)+1); (*packet)++;
    **packet = *(((uint8_t *)dest)+0); (*packet)++;
#endif
}

static inline void GetnB(uint8_t **packet, void *dest, uint32_t n)
{
    memcpy(dest, *packet, n);
    (*packet) += n;
}
static inline void PutnB(void *dest, uint8_t **packet, uint32_t n)
{
    memcpy(*packet, dest, n);
    (*packet) += n;
}

#endif
