#ifndef _HASHMAP_H_
#define _HASHMAP_H_

#ifdef __cplusplus
extern "C" {
#endif
#include  <stdint.h>

    typedef void *      dht_hashmap_t;
    typedef uint64_t    dht_key_t;
    typedef void *      value_t;

    dht_hashmap_t hashmap_init();
    void hashmap_destroy(dht_hashmap_t untyped_ptr);
    uint8_t hashmap_lookup(dht_hashmap_t untyped_self, dht_key_t key, value_t *pvalue);
    void hashmap_update(dht_hashmap_t untyped_self, dht_key_t key, value_t value);
    void hashmap_erase(dht_hashmap_t untyped_self, dht_key_t key);
    void hashmap_reset(dht_hashmap_t untyped_self);
    uint8_t hashmap_iterator(dht_hashmap_t untyped_self, void iterator_cb(dht_key_t, value_t, dht_key_t), dht_key_t key);

#ifdef __cplusplus
}
#endif
#endif //_HASHMAP_H_
