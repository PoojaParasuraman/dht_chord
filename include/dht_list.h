#ifndef P2P_LIST_H
#define P2P_LIST_H

#include <stdio.h>
#include <stdint.h>

typedef struct p2p_node_s p2p_node_t;

struct p2p_node_s {
    p2p_node_t *prev;
    p2p_node_t *next;
    void *data;
};

typedef struct p2p_list_s {
    uint16_t count;
    p2p_node_t *head;
    p2p_node_t *tail;
} p2p_list_t;

typedef struct p2p_list_parser_s {
    p2p_list_t* list;
    p2p_node_t* node;
} p2p_list_parser_t;

p2p_list_t *create_list();
p2p_node_t *create_node (void *data); 

void *peek_first_data(p2p_list_t *list);
void *peek_last_data(p2p_list_t *list);
void *find_object (p2p_list_t * list, void* find_obj, int (*is_equal)(void* obj, void* find_obj));
void * object_at_index(p2p_list_t* plist, int position);
int insert_first (p2p_list_t *plist, void *data);
int insert_last (p2p_list_t *plist, void *data);
int insert_at_position (p2p_list_t *plist, void *data, int index);
void *compare_and_remove (p2p_list_t* plist, void *data, int compare(void *node_data, void *data));
void *remove_first (p2p_list_t *plist);
void *remove_key (p2p_list_t* plist, uint8_t *key);
void *remove_last (p2p_list_t* plist);
void attach_list_parser (p2p_list_parser_t *p, p2p_list_t *plist);
p2p_list_parser_t *new_list_parser (p2p_list_t *plist);
void free_list_iterator(p2p_list_parser_t *p);
void* get_next_object (p2p_list_parser_t *p);
#endif
