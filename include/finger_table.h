#ifndef FINGER_TABLE_H
#define FINGE_TABLE_H

#include <stdio.h>

#include "dht_datamodel.h"
#include "dht_chord.h"
#include "dht_api.h"

#define SUCCESSOR_ID 0
#define MAX_NUM_SUCC 2
#define MAX_FT_SIZE  64

typedef enum ft_states {
    FT_PING_PRED   = 0,
    FT_SUCC_1      = 1,
    FT_SUCC_0      = 2,
    FT_FIND_SUCC   = 3,
    FT_COMPLETE    = 4,
} ft_states_t;

typedef struct dht_finger_entry_s {
    uint8_t         active;
    uint64_t        key;
    chord_node_data_t data;
} dht_finger_entry_t;

typedef struct dht_finger_table_s {
    uint64_t           local_key;
    dht_finger_entry_t pred; /*Predecessor*/
    dht_finger_entry_t successors[MAX_NUM_SUCC];
    dht_finger_entry_t entries[MAX_FT_SIZE]; /* entries[0] -> Successor */
} dht_finger_table_t;

void dump_finger_table();
int initialise_finger_table (uint64_t key, chord_node_data_t successor);
int find_index_from_key (uint64_t key);
int update_finger_entry (int index, chord_node_data_t *node);
int update_predecessor (chord_node_data_t *pred);
int is_my_pred (chord_node_data_t *pred);
void get_predecessor (chord_node_data_t *pred);
int is_successor_active();
int is_predecessor_active();
int is_key_beyond_succ (uint64_t key);
int update_successor_list (chord_node_data_t *succ);
int is_my_successor (chord_node_data_t *succ);
void get_successor (chord_node_data_t *succ);
void get_node_entry (int pos, chord_node_data_t *node);
int move_and_get_next_succ (chord_node_data_t *node);
void get_last_succ (chord_node_data_t *node);
void get_finger_key (int pos, uint64_t *key);
void remove_predecessor ();
int is_local_node_successor (uint64_t key);
int is_true_successor (uint64_t key);
int is_only_node();
int is_only_one_succ();
int is_found_only_succ();
int is_only_bootstrapper_present();
int is_succ_found(int index);
int update_last_succ(chord_node_data_t data);
int find_successor (uint64_t key, chord_node_data_t *data);

#endif
