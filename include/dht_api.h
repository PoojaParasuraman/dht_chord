#ifndef API_DATA_MODEL_H
#define API_DATA_MODEL_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <uv.h>

#include "dht_datamodel.h"
#include "dht_list.h"

#define MAX_CLIENTS             16
#define MAX_PACKET_LEN       65536
#define MAX_KEY_LEN             32
#define DHT_PUT_MSG_HDR_LEN     40
#define DHT_SUCCESS_MSG_HDR_LEN 36
#define DHT_FAILURE_MSG_LEN     36
#define DHT_PUT_RESP_HDR_LEN    16
#define DHT_GET_MSG_LEN         36
#define MAX_SOCK_PATH_LEN       32
#define MAX_QUERY_TIMEOUT       10
#define ARRAY_LEN(data_struct, struct_definition) ((int) (sizeof(data_struct)/sizeof(struct_definition)))

typedef enum api_msg_types_s {
    DHT_PUT      = 650,
    DHT_GET      = 651,
    DHT_SUCCESS  = 652,
    DHT_FAILURE  = 653,
    PUT_RESPONSE = 1001,
} api_msg_types_t;

typedef struct api_req_node_s {
    int              fd;
    struct timespec  req_time;
    uint8_t          key[MAX_KEY_LEN];
} api_req_node_t;

typedef struct api_put_data_s {
    uint8_t   key[MAX_KEY_LEN];
    uint64_t  value_len;
    uint8_t   *value;
} api_put_data_t;

typedef struct api_put_s {
    uint16_t       ttl;
    uint8_t        replication;
    api_put_data_t put_data;
} api_put_t;

typedef struct api_get_s {
    uint8_t  key[MAX_KEY_LEN];
} api_get_t;

typedef struct api_message_s {
    int         fd;
    uint16_t    len;
    uint16_t    type;
    void       *data;
} api_message_t;

typedef struct api_msg_cb_obj_s {
    uint16_t type;
    int (*validation_cb)(api_message_t *recv_msg);
    int (*action_cb)(api_message_t *recv_msg);
} api_msg_cb_obj_t;

typedef struct api_data_s {
    dht_node_data_t api_node;
    int             local_client_fd;
    int             server_fd;
    char            server_path[MAX_SOCK_PATH_LEN];
    p2p_list_t      *req_list;
    uv_loop_t       *loop;
    uv_timer_t      periodic_timer;
    uv_poll_t       server_handle;
    uv_poll_t       local_client_handle;
    p2p_list_t      *poll_handle_list;
} api_data_t;

typedef struct dht_api_data_s {
    dht_node_data_t  api_node;
    char             path[MAX_SOCK_PATH_LEN];
}dht_api_data_t;

int dht_init_api_thread(dht_node_data_t data, const char *path);

#endif
