#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#include "dht_api.h"
#include "dht_utils.h"
#include "dht_list.h"
#include "dht_datamodel.h"
#include "dht_callbacks.h"

#ifndef DHT_CHORD_H
#define DHT_CHORD_H

#define MAX_CHORD_CLIENTS     256
#define MAX_HOP_COUNT           3
#define MAX_FT_TIMEOUT          5
#define MAX_CHORD_KEY_LEN       8
#define CHORD_IPV4         0x0001

#define CHORD_ORIGINAL 0
#define CHORD_FORWARD  1

#define CHORD_PING_PRED_SUCCESS 0
#define CHORD_PING_PRED_FAILURE 1

#define MAX_CHORD_HEADER_LEN         12
#define MAX_PRED_RESP_BODY_LEN        8
#define MIN_CHORD_PUT_REQ_BODY_LEN   36
#define MAX_CHORD_SUCC_RESP_BODY_LEN 24

typedef struct chord_node_data_s chord_node_data_t;
typedef struct chord_successor_resp_s chord_successor_resp_t;
typedef struct chord_put_request_s chord_put_request_t;
typedef struct chord_get_req_data_s chord_get_req_data_t;
typedef struct chord_send_get_req_data_s chord_send_get_req_data_t;
typedef struct chord_get_response_s chord_get_response_t;
typedef struct chord_req_node_s chord_req_node_t;
typedef struct chord_msg_cb_obj_s chord_msg_cb_obj_t;
typedef struct chord_msg_s chord_msg_t;
typedef struct chord_data_s chord_data_t;
typedef struct dht_chord_data_s dht_chord_data_t;
typedef struct dht_fd_obj_s dht_fd_obj_t;

typedef enum chord_msg_types_s {
    DHT_CHORD_PUT_REQUEST   = 7001,
    DHT_CHORD_GET_REQUEST   = 7002,
    DHT_CHORD_GET_RESPONSE  = 7003,
    DHT_CHORD_SHARE_DATA    = 7004,
} chord_msg_types_t;

typedef enum c2c_msg_types_s {
    CHORD_NOTIFY_SUCCESSOR     = 2001,
    CHORD_PING_PREDECESSOR     = 2002,
    CHORD_SUCCESSOR_REQUEST    = 2003,
    CHORD_SUCCESSOR_RESPONSE   = 2004,
    CHORD_PREDECESSOR_REQUEST  = 2005,
    CHORD_PREDECESSOR_RESPONSE = 2006,
    CHORD_PUT_REQUEST          = 2007,
    CHORD_GET_REQUEST          = 2008,
    CHORD_GET_RESPONSE         = 2009,
    CHORD_ACK                  = 2010,
} c2c_msg_types_t;

struct chord_node_data_s {
    uint64_t key;
    uint16_t port;
    uint8_t  protocol;
    union {
        char            hostname[MAX_HOSTNAME_LEN];
        struct in_addr  ipv4;
        struct in6_addr ipv6;
    };
};

struct chord_successor_resp_s {
    uint64_t          request_key;
    chord_node_data_t data;
};

struct chord_put_request_s {
    uint8_t   key[MAX_KEY_LEN];
    uint16_t  ttl;
    uint8_t   replica;
    uint64_t  value_len;
    uint8_t   *value;
};

struct chord_get_req_data_s {
    chord_node_data_t node;
    uint8_t           key[MAX_KEY_LEN];
};

struct chord_get_response_s {
    uint8_t   key[MAX_KEY_LEN];
    uint64_t  value_len;
    uint8_t   *value;
};

struct chord_send_get_req_data_s {
    chord_node_data_t    node;
    chord_get_response_t data;
};

struct chord_req_node_s {
    chord_node_data_t   succ;
    struct timespec     req_time;
    chord_put_request_t put_data;
};

struct chord_msg_cb_obj_s {
    uint16_t type;
    int (*action_cb)(chord_msg_t *recv_msg);
};

struct dht_fd_obj_s {
    int               fd;
    chord_node_data_t node;
};

struct chord_msg_s {
    int               fd;
    uint16_t          len;
    uint16_t          type;
    chord_node_data_t node;
    void              *data;
};

struct chord_data_s {
    chord_node_data_t  chord_node;
    chord_node_data_t  bootstrap_node;
    int                local_client_fd;
    int                server_fd;
    char               server_path[MAX_SOCK_PATH_LEN];
    uv_loop_t          *loop;
    uv_timer_t         periodic_timer;
    uv_poll_t          server_handle;
    uv_poll_t          local_client_handle;
    p2p_list_t         *poll_handle_list;
    p2p_list_t         *fd_list;
    struct timespec    stabilise_time;
    bool               ack;
    int                stabilise_status;
    int                stabilise_entry;
};

typedef struct dht_chord_data_s {
    dht_node_data_t  chord_node;
    dht_node_data_t  bootstrap_node;
    char             path[MAX_SOCK_PATH_LEN];
} dht_chord_data_t;

int dht_init_chord_thread(dht_node_data_t ch, dht_node_data_t bt, const char *path);

#endif
