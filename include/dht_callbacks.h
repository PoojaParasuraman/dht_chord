#ifndef DHT_CALLBACKS_H
#define DHT_CALLBACKS_H

#include <stdio.h>
#include <stdint.h>

#include "dht_datamodel.h"
#include "dht_api.h"

typedef struct dht_msg_cb_obj_s {
    uint16_t type;
    int (*validation_cb)(dht_notify_data_t *recv_msg);
    int (*action_cb)(dht_notify_data_t *recv_msg);
} dht_msg_cb_obj_t;

typedef struct dht_hash_entry_s {
    struct timespec  req_time;
    api_put_t        *data;
} dht_hash_entry_t;

void uvpoll_dht_read_cb (uv_poll_t* handle, int status, int events);
int dht_thread_server_init (int *sockfd);
void dht_check_and_share_data (dht_key_t hash_key, value_t value, dht_key_t key);

#endif
