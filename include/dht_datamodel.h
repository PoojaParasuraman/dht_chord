#ifndef DHT_DATA_MODEL_H
#define DHT_DATA_MODEL_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <uv.h>

#include "dht_hashmap.h"

#define MAX_HOSTNAME_LEN 255
#define DHT_SOCK_PATH    "dht_sock_path"
#define MAX_DHT_CLIENTS  2

typedef enum dht_protocol_s {
    DHT_IPV4     = 0x01,
    DHT_IPV6     = 0x02,
    DHT_HOSTNAME = 0x04
} dht_protocol_t;

typedef struct dht_node_data_s {
    uint64_t            key;
    uint16_t            port_number;
    dht_protocol_t      protocol;
    union {
	char            hostname[MAX_HOSTNAME_LEN];
        struct in_addr  ipv4;
        struct in6_addr ipv6;
    };
} dht_node_data_t;

typedef struct dht_data_s {
    dht_node_data_t  api_node;
    dht_node_data_t  p2p_node;
    dht_node_data_t  bootstrapper;
    const char       *config_file;
    const char       *server_path;
    dht_hashmap_t    hashmap;
    uv_loop_t        *loop;
    uv_timer_t       periodic_timer;
    uv_poll_t        server_handle;
    int              client_count;
    uv_poll_t        client_handles[MAX_DHT_CLIENTS];
    int              fds[MAX_DHT_CLIENTS];
    int              server_fd;
    int              api_fd;
    int              chord_fd;
} dht_data_t;

typedef struct dht_notify_data_s {
    uint16_t type;
    void *data;
} dht_notify_data_t;

#endif
