PHOENIX DHT CHORD

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
How to Build and Run:
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

The following steps need to be followed in-order to compile and successfully run the project.

1. Install the following packages
	sudo apt-get update
	sudo apt install cmake
	sudo apt-get install build-essential
	sudo apt-get install doxygen
	sudo apt-get install cppcheck
	sudo apt-get install graphviz
	sudo apt-get install libuv1-dev
	sudo apt-get install libssl-dev
	sudo apt-get install valgrind

2. Clone the repository using the below command
	git clone https://gitlab.lrz.de/netintum/teaching/p2psec_projects_2020/dht-2.git

3. cd dht-2

4. cmake .

5. make

6. ./dhtchord -c <Config_file> -p <UDP_SOCK_PATH>
	./dhtchord -c Config.ini -p first_node
7. In another terminal, start test app from the same path as
	./test/dhtchord_app -a <ip_address> -p <port>
	Eg: ./dhtchord_app -a 127.0.0.1 -p 55001
8. Similarly start the second node
	Eg: ./dhtchord -c Config1.ini -p second_node
9. In another terminal, start test app from the same path as
	./test/dhtchord_app -a <ip_address> -p <port>
	Eg: ./dhtchord_app -a 127.0.0.1 -p 53001
10. On the terminal of dhtchord test apps, type “1” for “put” or type “2” for “get” data.

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Note:
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

1. To run the application using Valgrind, execute the following command 
	valgrind -v --tool=memcheck --leak-check=full --leak-resolution=high --showleak-kinds=definite --main-stacksize=12800000 ./dhtchord -c Config.ini -p first_node

2. Many Test apps for the same chord node can be created using the same server port and IP.

3. Many Chord nodes can be created by using different Config files. The first node in the network must have the listen_address and bootstrapper to be the same.

4. Sample Config files have been provided for testing purpose.

5. Test results of Valgrind is attached as “Valgrind_Report.txt”

6. Doxygen generated HTML file will be available at docs/html folder after building the project

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Limitations:
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

1. The application uses 64-bit hash key and hence cannot handle more than 2^64 chord nodes or data entries.

2. The stabilization time per chord node addiction is around ~1 minute.

3. Testing has been done with 3 chord nodes so far and it works as expected. More chord nodes addition hasn’t test yet.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
