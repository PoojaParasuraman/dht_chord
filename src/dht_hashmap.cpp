#include "dht_hashmap.h"
#include "dht_hashmap_class.h"

extern "C" {

dht_hashmap_t hashmap_init() {
    return new Hashmap();
}

void hashmap_destroy(dht_hashmap_t untyped_ptr) {
    Hashmap* typed_ptr = static_cast<Hashmap*>(untyped_ptr);
    delete typed_ptr;
}

uint8_t hashmap_lookup(dht_hashmap_t untyped_self, dht_key_t key, value_t *pvalue) {
    Hashmap* typed_self = static_cast<Hashmap*>(untyped_self);
    uint8_t ret;
    ret = typed_self->lookup(key, pvalue);
    return ret;
}

void hashmap_update(dht_hashmap_t untyped_self, dht_key_t key, value_t value) {
    Hashmap* typed_self = static_cast<Hashmap*>(untyped_self);
    typed_self->update(key, value);
}

void hashmap_erase(dht_hashmap_t untyped_self, dht_key_t key) {
    Hashmap* typed_self = static_cast<Hashmap*>(untyped_self);
    typed_self->erase(key);
}

void hashmap_reset(dht_hashmap_t untyped_self) {
    Hashmap* typed_self = static_cast<Hashmap*>(untyped_self);
    typed_self->reset();
}

uint8_t hashmap_iterator(dht_hashmap_t untyped_self, void iterator_cb(dht_key_t, value_t, dht_key_t), dht_key_t key) {
    Hashmap* typed_self = static_cast<Hashmap*>(untyped_self);
    return typed_self->iterate(iterator_cb, key);
}
}

