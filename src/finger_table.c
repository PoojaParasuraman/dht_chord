#include "finger_table.h"
#include <stdio.h>
#include <math.h>
#include <string.h>

static pthread_rwlock_t ft_lock = PTHREAD_RWLOCK_INITIALIZER;
dht_finger_table_t      g_ft    = {0};

int initialise_finger_table (uint64_t key, chord_node_data_t successor) {
    pthread_rwlock_wrlock(&ft_lock);

    memset(&g_ft, 0, sizeof(dht_finger_table_t));

    g_ft.local_key = key;
    g_ft.entries[SUCCESSOR_ID].active = 1;
    get_finger_key(SUCCESSOR_ID, &g_ft.entries[SUCCESSOR_ID].key);
    memcpy (&g_ft.entries[SUCCESSOR_ID].data, &successor, sizeof(chord_node_data_t));
    g_ft.successors[SUCCESSOR_ID].active = 1;
    memcpy (&g_ft.successors[SUCCESSOR_ID].data, &successor, sizeof(chord_node_data_t));

    pthread_rwlock_unlock(&ft_lock);
    return 0;
}

void dump_finger_table() {
    int i = 0;
    pthread_rwlock_wrlock(&ft_lock);

    printf("\n------------------- FINGER TABLE DUMP ---------------------");
    printf("\n Predecessor: ACTIVE - %d,  Key - %lu,   Node Key - %lu",g_ft.pred.active,g_ft.pred.key,g_ft.pred.data.key);
    printf("\n------------------------------------------------------------");
    printf("\n Successor A: ACTIVE - %d,  Key - %lu,   Node Key - %lu",g_ft.successors[0].active,g_ft.successors[0].key,g_ft.successors[0].data.key);
    printf("\n Successor B: ACTIVE - %d,  Key - %lu,   Node Key - %lu",g_ft.successors[1].active,g_ft.successors[1].key,g_ft.successors[1].data.key);
    printf("\n------------------------------------------------------------");
    printf("\n Successor 0: ACTIVE - %d,  Key - %lu,   Node Key - %lu",g_ft.entries[0].active,g_ft.entries[0].key,g_ft.entries[0].data.key);

    for (i = 1; i < MAX_FT_SIZE; i++) {
        if (!g_ft.entries[i].active) {
            break;
	}
        printf("\n------------------------------------------------------------");
	printf("\n Successor %d: ACTIVE - %d,  Key - %lu,   Node Key - %lu",i,g_ft.entries[i].active,g_ft.entries[i].key,g_ft.entries[i].data.key);
    }
    printf("\n===========================================================\n");
    pthread_rwlock_unlock(&ft_lock);
}

int find_index_from_key (uint64_t key) {
   int index = -1;

   index = ceil(log2(key - g_ft.local_key));

   return (index < MAX_FT_SIZE) ? index : MAX_FT_SIZE;
}

int update_finger_entry (int index, chord_node_data_t *node) {

    if (!node) {
        printf("dht_successor_resp_t is NULL");
        return -1;
    }

    pthread_rwlock_wrlock(&ft_lock);

    if (index == SUCCESSOR_ID) {
        memcpy(&g_ft.successors[SUCCESSOR_ID].data, node, sizeof(chord_node_data_t));
    }
    if ((index >= 0) && (index < MAX_FT_SIZE)) {
	g_ft.entries[index].active = 1;
	get_finger_key(index, &g_ft.entries[index].key);
	memcpy(&g_ft.entries[index].data, node, sizeof(chord_node_data_t));
    }
    pthread_rwlock_unlock(&ft_lock);
    return 0;
}

int update_predecessor (chord_node_data_t *pred) {
    if (!pred) {
        printf("dht_successor_resp_t is NULL");
        return -1;
    }

    pthread_rwlock_wrlock(&ft_lock);
    g_ft.pred.active = 1;
    memcpy(&g_ft.pred.data, pred, sizeof(chord_node_data_t));
    pthread_rwlock_unlock(&ft_lock);

    return 0;
}

void get_predecessor (chord_node_data_t *pred) {

    if (!pred) {
        printf("\n%s: Pred is NULL", __func__);
        return;
    }

    pthread_rwlock_wrlock(&ft_lock);
    memcpy(pred, &g_ft.pred.data, sizeof(chord_node_data_t));
    pthread_rwlock_unlock(&ft_lock);

    return;
}

void get_successor (chord_node_data_t *succ) {

    if (!succ) {
        printf("\n%s: Succ is NULL", __func__);
        return;
    }

    pthread_rwlock_wrlock(&ft_lock);
    memcpy(&g_ft.entries[SUCCESSOR_ID].data, succ, sizeof(chord_node_data_t));
    pthread_rwlock_unlock(&ft_lock);

    return;
}

void get_node_entry (int pos, chord_node_data_t *node) {

    if (!node) {
        printf("\n%s: Node is NULL", __func__);
        return;
    }

    pthread_rwlock_wrlock(&ft_lock);
    memcpy(node, &g_ft.entries[pos].data, sizeof(chord_node_data_t));
    pthread_rwlock_unlock(&ft_lock);

    return;
}

int move_and_get_next_succ (chord_node_data_t *node) {
    int i      = 0;
    int status = 0;

    if (!node) {
        printf("\n%s: node is NULL", __func__);
        return 0;
    }

    pthread_rwlock_wrlock(&ft_lock);
    for (i = 0; i < MAX_NUM_SUCC-1; i++) {
        memcpy(&g_ft.successors[i], &g_ft.successors[i+1], sizeof(dht_finger_entry_t));
    }
    memset(&g_ft.successors[i], 0, sizeof(dht_finger_entry_t));

    if (g_ft.successors[SUCCESSOR_ID].active) {
        memcpy(&g_ft.entries[SUCCESSOR_ID], &g_ft.successors[0], sizeof(dht_finger_entry_t));
        memcpy(node, &g_ft.entries[SUCCESSOR_ID].data, sizeof(chord_node_data_t));
	status = 1;
    }

    pthread_rwlock_unlock(&ft_lock);
    return status;
}

int update_successor_list (chord_node_data_t *succ) {
    int i = 0;

    if (!succ) {
        printf("\n%s: succ is NULL", __func__);
        return i;
    }

    pthread_rwlock_wrlock(&ft_lock);
    for (i = 0; i < MAX_NUM_SUCC; i++) {
	if (!g_ft.successors[i].active) {
	    g_ft.successors[i].active = 1;

            memcpy(&g_ft.successors[i].data, succ, sizeof(chord_node_data_t));
	}
    }
    pthread_rwlock_unlock(&ft_lock);
    return i;
}

void get_last_succ (chord_node_data_t *node) {
    int i = 0;

    if (!node) {
        printf("\n%s: node is NULL", __func__);
        return;
    }

    pthread_rwlock_wrlock(&ft_lock);
    for (i = MAX_NUM_SUCC-1; i >= 0; i--) {
	if (g_ft.successors[i].active) {
            memcpy(node, &g_ft.successors[i].data, sizeof(chord_node_data_t));
	    break;
	}
    }
    pthread_rwlock_unlock(&ft_lock);
    return;
}

void get_finger_key (int pos, uint64_t *key) {
    if (!key) {
        printf("\n%s: key is NULL", __func__);
        return;
    }
    uint64_t index = (int) pow((double) 2, (double)pos);
    *key = g_ft.local_key + (uint64_t) index;

    return;
}

int is_my_pred (chord_node_data_t *pred) {
    int status = 0;

    if (!pred) {
        printf("\n%s: pred is NULL", __func__);
        return status;
    }

    pthread_rwlock_rdlock(&ft_lock);
    if (g_ft.pred.active && (0 == memcmp(pred, &g_ft.pred.data, sizeof(chord_node_data_t))))
        status = 1;
    pthread_rwlock_unlock(&ft_lock);

    return status;
}

int is_predecessor_active() {
    int status = 0;

    pthread_rwlock_rdlock(&ft_lock);
    if (g_ft.pred.active)
        status = 1;
    pthread_rwlock_unlock(&ft_lock);

    return status;
}

int is_successor_active() {
    int status = 0;

    pthread_rwlock_rdlock(&ft_lock);
    if (g_ft.entries[SUCCESSOR_ID].active)
        status = 1;
    pthread_rwlock_unlock(&ft_lock);

    return status;
}

int is_my_successor (chord_node_data_t *succ) {
    int status = 0;

    if (!succ) {
        printf("\n%s: succ is NULL", __func__);
        return status;
    }

    pthread_rwlock_rdlock(&ft_lock);
    if (g_ft.entries[SUCCESSOR_ID].active && (0 == memcmp(succ, &g_ft.entries[SUCCESSOR_ID].data, sizeof(chord_node_data_t))))
        status = 1;
    pthread_rwlock_unlock(&ft_lock);

    return status;
}

int is_key_beyond_succ (uint64_t key) {
    int status        = 0;
    uint64_t succ_key = g_ft.entries[SUCCESSOR_ID].data.key;

    if (succ_key > key) {
        if ((succ_key < MAX_FT_SIZE) && ((MAX_FT_SIZE+key) > succ_key)) {
            status = 1;
	}
    }
    else {
	status = 1;
    }
    return status;
}

void remove_predecessor () {

    pthread_rwlock_wrlock(&ft_lock);
    memset(&g_ft.pred, 0, sizeof(dht_finger_entry_t));
    pthread_rwlock_unlock(&ft_lock);
    return;
}

int is_local_node_successor (uint64_t key) {
    int status = 0;

    pthread_rwlock_rdlock(&ft_lock);
    if ((key > g_ft.pred.data.key) && (key <= g_ft.local_key))
        status = 1;

    pthread_rwlock_unlock(&ft_lock);
    return status;
}

int is_true_successor (uint64_t key) {
    int status = 0;
    pthread_rwlock_rdlock(&ft_lock);
    if (key <= g_ft.entries[MAX_FT_SIZE-1].data.key)
        status = 1;

    pthread_rwlock_unlock(&ft_lock);
    return status;
}

int is_only_node() {
    int status = 0;

    pthread_rwlock_rdlock(&ft_lock);
    if (((1 == g_ft.pred.active) && (g_ft.local_key != g_ft.pred.data.key)) && ((1 == g_ft.entries[SUCCESSOR_ID].active) && (g_ft.local_key == g_ft.entries[SUCCESSOR_ID].data.key)))
        status = 1;

    pthread_rwlock_unlock(&ft_lock);
    return status;
}

int is_only_one_succ() {
    int i      = 0;
    int status = 1;

    pthread_rwlock_wrlock(&ft_lock);
    for (i = MAX_NUM_SUCC-1; i > 0; i--) {
        if (g_ft.successors[i].active) {
	    status = 0;
            break;
        }
    }

    pthread_rwlock_unlock(&ft_lock);

    return status;
}

int is_found_only_succ() {
    int status = 0;

    pthread_rwlock_rdlock(&ft_lock);

    if ((0 == g_ft.pred.active) && (1 == g_ft.entries[SUCCESSOR_ID].active) &&
	(g_ft.local_key != g_ft.entries[SUCCESSOR_ID].data.key) &&
	(0 == g_ft.entries[SUCCESSOR_ID+1].active))
        status = 1;

    pthread_rwlock_unlock(&ft_lock);
    return status;
}

int is_only_bootstrapper_present() {
    int status = 0;

    pthread_rwlock_rdlock(&ft_lock);

    if ((1 == g_ft.pred.active) && (1 == g_ft.entries[SUCCESSOR_ID].active) &&
        (g_ft.pred.data.key == g_ft.entries[SUCCESSOR_ID].data.key))
        status = 1;

    pthread_rwlock_unlock(&ft_lock);
    return status;
}

int is_succ_found(int index) {
    int status = 0;

    pthread_rwlock_rdlock(&ft_lock);

    if (1 == g_ft.entries[index].active)
        status = 1;

    pthread_rwlock_unlock(&ft_lock);
    return status;
}

int find_successor (uint64_t key, chord_node_data_t *data) {
    int i     = 0;
    int index = -1;

    if (!data) {
	printf("Input params are NULL");
        return -1;
    }

    index = find_index_from_key(key);

    pthread_rwlock_rdlock(&ft_lock);
    if (index > 0) {
        if (index >= MAX_FT_SIZE)
	    index = MAX_FT_SIZE-1;

	for (i = index; i >= 0; i--) {
	    if (g_ft.entries[i].active) {
                memcpy (data, &g_ft.entries[i].data, sizeof(chord_node_data_t));
		break;
	    }
	}
    }

    pthread_rwlock_unlock(&ft_lock);
    return index;
}
