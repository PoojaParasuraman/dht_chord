#include "dht_api.h"
#include "dht_list.h"
#include "dht_datamodel.h"
#include "dht_utils.h"

#include <uv.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

api_data_t gdata = {0};

int api_process_get_msg(api_message_t *recv_msg);
int api_process_put_request(api_message_t *recv_msg);
int api_process_put_response(void *buff);
int api_send (int fd, void *buf, int size);
void api_msg_data_free(api_message_t *msg);
int dht_thread_connect_to_main (int *sockfd);
int api_thread_server_init (int *sockfd);

api_msg_cb_obj_t api_msg_cb_obj[] = {
    {
        .type      = DHT_GET,
        .action_cb = api_process_get_msg,
    },
    {
        .type      = DHT_PUT,
        .action_cb = api_process_put_request,
    },
};

int api_send (int fd, void *buf, int size) {
    if (-1 != fd) {
        int len = 0;
        if (-1 == (len = send(fd, buf, size, 0))){
            printf("\n%s: send failed", __func__);
            return -1;
        }
	if (len != size) {
            printf("\n%s: Mismatch of bytes sent : %d,%d",__func__,size,len);
	    return -1;
	}
        return 0;
    }
    return -1;
}

void api_msg_data_free(api_message_t *msg) {
    if(msg && msg->data) {
        free(msg->data);
    }
    return;
}

void periodic_timer_cb (uv_timer_t *handle) {
    api_req_node_t  *req_data  = NULL;
    p2p_list_t      *plist     = NULL;
    struct timespec  curr_time = {0};

    curr_time = get_current_time();
    plist     = gdata.req_list;

   if(!handle) {
     printf("\n%s: Handle is NULL", __func__);
   }

   if (!plist)
       return;

   req_data = (api_req_node_t *) object_at_index (plist, 0);
   while (req_data)
   {
       if (MAX_QUERY_TIMEOUT > get_clock_diff_secs(curr_time, req_data->req_time))
           return;
       else
       {
           req_data = (api_req_node_t *) remove_first(plist);
           if (req_data)
           {
              /* Send Failure message */
              uint8_t *msg,*p = NULL;
              uint16_t len    = 0;
              uint16_t type   = 0;

              len  = DHT_FAILURE_MSG_LEN;
              type = DHT_FAILURE;

              /* Construct and send payload */
              msg = p = (uint8_t *) calloc (1, len);
              if (!msg) {
                  printf("\n%s : Calloc failure for msg",__func__);
		  free(req_data);
                  break;
              }

              Put2B(&len,  &p);
              Put2B(&type, &p);
              PutnB(req_data->key, &p, MAX_KEY_LEN);

              if (-1 == api_send(req_data->fd, msg, len)) {
                  printf("\n%s: dht_send() failed for DHT_FAILURE", __func__);
              }

              free(msg);
              free(req_data);
           }
       }
       req_data = (api_req_node_t *) object_at_index (plist, 0);
   }
   return;
}

int compare_req_node (void *a, void *b) {
    if (a && b) {
        api_req_node_t *find_obj = (api_req_node_t *)a;
        api_req_node_t *obj      = (api_req_node_t *)b;

	if ((find_obj->fd == obj->fd) &&
            (0 == memcmp(find_obj->key, obj->key, MAX_KEY_LEN)))
            return 1;
    }
    return 0;
}

int api_process_get_msg(api_message_t *recv_msg) {
    api_req_node_t *obj      = NULL;
    api_req_node_t *find_obj = NULL;

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    uint8_t *key = (uint8_t *) recv_msg->data;

    if (!key) {
        printf("\n%s : Get data is NULL",__func__);
        return -1;
    }

    uint8_t *req_key = (uint8_t *) calloc (1, MAX_KEY_LEN);

    if (!req_key)
	return -1;

    memcpy(req_key, key, MAX_KEY_LEN);

    /* Send data */
    if (-1 == dht_sendto_thread ((void *)req_key, DHT_GET, gdata.local_client_fd)) {
        printf("\n%s : Failed to send data to main thread\n",__func__);
	free(req_key);
	return -1;
    }

    /* add to list */
    obj = (api_req_node_t *) calloc (1, sizeof(api_req_node_t));
    if (!obj) {
	printf("\n%s: Calloc failed for list node",__func__);
	return -1;
    }

    memcpy(obj->key, key, MAX_KEY_LEN);
    obj->fd       = recv_msg->fd;
    obj->req_time = get_current_time();

    /* check if node exists already */
    find_obj = (api_req_node_t *) find_object (gdata.req_list, obj, compare_req_node);
    if (find_obj) {
	find_obj->req_time = get_current_time();
        free(obj);
    }
    else {
        if (-1 == insert_last(gdata.req_list, (void *)obj)) {
    	    printf("\n%s: Insert node at last failed",__func__);
            free(obj);
            return -1;
	}
    }

    return 0;
}

int api_process_put_request(api_message_t *recv_msg) {

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    api_put_t *put_data = (api_put_t *) recv_msg->data;

    if (!put_data) {
        printf("\n%s : value of put data is NULL",__func__);
        return -1;
    }

    /* Send data */
    api_put_t *pt_data = (api_put_t *) calloc (1, sizeof(api_put_t));

    memcpy(pt_data, put_data, sizeof(api_put_t));

    if (-1 == dht_sendto_thread ((void *)pt_data, DHT_PUT, gdata.local_client_fd)) {
        printf("\n%s : Failed to send data to main thread",__func__);
        return -1;
    }

    return 0;
}

int compare_key (void *node_data, void *data) {
    api_req_node_t *ndata = (api_req_node_t *) node_data;
    uint8_t        *key   = (uint8_t *) data;

    if (ndata && key) {
	if (0 == (memcmp(ndata->key, key, MAX_KEY_LEN))) {
            return 1;
	}
    }
    return 0;
}

int api_process_put_response(void *buff) {
    uint8_t *msg,*p = NULL;
    uint16_t len    = 0;
    uint16_t type   = 0;
    int status      = 0;

    api_put_data_t *data = (api_put_data_t *) buff;
    if (!data) {
        printf("\n%s : Put data is NULL",__func__);
        return -1;
    }

    if ((data->value_len > 0) && (!data->value)) {
	printf("\n%s : value of put data is NULL",__func__);
        return -1;
    }

    len  = DHT_SUCCESS_MSG_HDR_LEN + data->value_len;
    type = DHT_SUCCESS;

    /* Construct and send payload */
    msg = p = (uint8_t *) calloc (1, len);
    if (!msg) {
        printf("\n%s : Calloc failure for msg",__func__);
        return -1;
    }
    Put2B(&len,  &p);
    Put2B(&type, &p);
    PutnB(data->key, &p, MAX_KEY_LEN);
    if (data->value_len > 0) {
        PutnB(data->value, &p, data->value_len);
    }

    if (gdata.req_list) {
	p2p_node_t *node = gdata.req_list->head;
	while (node && node->data) {
            api_req_node_t *ndata = node->data;
            if (0 == memcmp(ndata->key, data->key, MAX_KEY_LEN)) {
                if (-1 == api_send(ndata->fd, msg, len)) {
                    printf("\n%s: dht_send() failed for DHT_SUCCESS", __func__);
                    status = -1;
		}
	    }
	    node = node->next;
	}
    }
    /* Delete nodes */
    compare_and_remove (gdata.req_list, data->key, compare_key);

    if (data->value)
	free(data->value);
    free(msg);
    return status;
}

int api_parse_msg(int fd, uint8_t *buf, api_message_t *msg, int len) {
    uint8_t reserved = 0;
 
    if (!buf || !msg) {
        printf("\n%s : Invalid input",__func__);
        return -1;
    }

    Get2B(&buf, &msg->len);

    if (msg->len != len) {
        printf("\n%s : Length Mismatch %d, actual-%d",__func__,len, msg->len);
        return -1;
    }
    Get2B(&buf, &msg->type);
    msg->fd = fd;

    switch(msg->type) {
        case DHT_PUT: {

            if (msg->len < DHT_PUT_MSG_HDR_LEN) {
                printf("\n%s : Invalid length for DHT_PUT message",__func__);
                return -1;
            }
            api_put_t *put_msg = (api_put_t *) calloc (1, sizeof(api_put_t));

            if (!put_msg) {
                printf("\n%s : Calloc failed for put_msg",__func__);
                return -1;
            }

            Get2B(&buf, &put_msg->ttl);
            Get1B(&buf, &put_msg->replication);
            Get1B(&buf, &reserved);
            GetnB(&buf, put_msg->put_data.key, MAX_KEY_LEN);

	    int data_len       = msg->len - DHT_PUT_MSG_HDR_LEN;
	    put_msg->put_data.value_len = data_len;
            if (data_len > 0) {
	        put_msg->put_data.value = (uint8_t *) calloc (1, data_len);
		if (!put_msg->put_data.value) {
                    printf("\n%s : Calloc failed for put_msg->data",__func__);
		    free(put_msg);
		    return -1;
		}
                GetnB(&buf, put_msg->put_data.value, data_len);
            }

	    msg->data = (void *) put_msg;
            break;
        }
        case DHT_GET: {

            if (msg->len != DHT_GET_MSG_LEN) {
                printf("\n%s : Invalid length for DHT_GET message",__func__);
                return -1;
            }

            uint8_t *key = (uint8_t *) calloc (1, sizeof(uint8_t)*MAX_KEY_LEN);

            if (!key) {
                printf("\n%s : Calloc failed for key",__func__);
                return -1;
            }

            GetnB(&buf, key, MAX_KEY_LEN);

            msg->data = (void *) key;
            break;
        }
        default: {
            printf("\n%s : Invalid message type - %d",__func__,msg->type);
	    return -1;
        }
    }
    return 0;
}

int api_get_msg_index(uint16_t type) {
    int index = 0;
    for (index = 0; index < ARRAY_LEN(api_msg_cb_obj, api_msg_cb_obj_t); index++) {
       if (api_msg_cb_obj[index].type == type)
           break;
    }

    if ( index >= ARRAY_LEN(api_msg_cb_obj, api_msg_cb_obj_t) ) {
        return -1;
    }
    return index;
}

int api_handle_message(int fd, uint8_t *buf, int msg_len) {
    api_msg_cb_obj_t *obj = NULL;
    api_message_t msg     = {0};
    int index             = -1;
    int status            = -1;

    do {
        if (-1 == api_parse_msg(fd, buf, &msg, msg_len)) {
            printf("\n%s : Incorrect msg format, failed to parse", __func__);
            break;
        }

        index = api_get_msg_index(msg.type);

        if (-1 == index) {
            printf("\n%s : Unknown message type %d", __func__,msg.type);
            break;
        }

        obj = &api_msg_cb_obj[index];
        if (!obj) {
            printf("\n%s: No object for index %d", __func__, index);
            break;
        }

        if (obj->validation_cb && (0 != obj->validation_cb(&msg))) {
            printf("validation failed for msg_type %d", msg.type);
            break;
        }

        if( (obj->action_cb != NULL) && (0 != obj->action_cb(&msg))) {
            printf("Action cb Error for msg_type %d", msg.type);
            break;
        }
        status = 0;
    } while (0);

    api_msg_data_free(&msg);

    return status;
}

int api_parse_server_msg (int sockfd) {
    int msg_len = 0;
    api_put_data_t buff = {0};

    if (sockfd != -1) {
        msg_len = recv(sockfd, &buff, sizeof(api_put_data_t), 0);

        if (-1 == msg_len) {
            printf("\n%s: read failure\n",__func__);
            return -1;
	}
	api_process_put_response (&buff);
    }
    return 0;
}

int api_msg_handle_cb(int sockfd) {
    int      msg_len                     = 0;
    uint8_t  sock_buffer[MAX_PACKET_LEN] = {0};

    if (sockfd != -1) {
        memset (sock_buffer, 0, MAX_PACKET_LEN);

        msg_len = read(sockfd, sock_buffer, MAX_PACKET_LEN);

        if (-1 == msg_len)
            return -1;

        sock_buffer[msg_len] = '\0';
        api_handle_message(sockfd, sock_buffer, msg_len);
    }
    return 0;
}

void uvpoll_read_cb (uv_poll_t* handle, int status, int events)
{
    int fd = -1;
    if (handle) {
	uv_fileno((uv_handle_t *)handle, &fd);
        if((status < 0) || (events & UV_DISCONNECT))
        {
            uv_poll_stop(handle);
            if (events & UV_DISCONNECT) {
	        if (fd == gdata.local_client_fd) {
	            close(gdata.local_client_fd);
                    int ret = dht_thread_connect_to_main(&gdata.local_client_fd);
                    while (ret < 0) {
                        ret = dht_thread_connect_to_main(&gdata.local_client_fd);
                    }
                    uv_poll_init  (gdata.loop, &gdata.local_client_handle, gdata.local_client_fd);
                    uv_poll_start (&gdata.local_client_handle, (UV_READABLE|UV_DISCONNECT), uvpoll_read_cb);
	        }
		else if (fd == gdata.server_fd) {
		    close(gdata.server_fd);
		    int ret = api_thread_server_init(&gdata.server_fd);
		    while (ret < 0) {
			ret = api_thread_server_init(&gdata.server_fd);
		    }
                    uv_poll_init  (gdata.loop, &gdata.server_handle, gdata.server_fd);
                    uv_poll_start (&gdata.server_handle, (UV_READABLE|UV_DISCONNECT), uvpoll_read_cb);
		}
		else {
                    /* Sort the handles */
	            compare_and_remove (gdata.poll_handle_list, (void *) handle, compare_handle_entry);
	            close(fd);
		}
	    }
        }
        else if (events & UV_READABLE) {
	    if (fd == gdata.server_fd) {

	        /* Accept connections from client */
	        int client_fd = -1;
                if (-1 == (client_fd = accept(gdata.server_fd, NULL,NULL))) {
                    printf("\n%s: accept client connection failed",__func__);
		    return;
	        }
	        /* Start uv poll for the client */
		uv_poll_t *handle = (uv_poll_t *) calloc (1, sizeof(uv_poll_t));

                uv_poll_init  (gdata.loop, handle, client_fd);
                uv_poll_start (handle, (UV_READABLE|UV_DISCONNECT), uvpoll_read_cb);

		if (-1 == insert_last(gdata.poll_handle_list, (void *)handle)) {
                    printf("\n%s: Insert node at last failed",__func__);
                    return;
                }
	    }
	    else if (fd == gdata.local_client_fd) {
	        api_parse_server_msg(fd);
	    }
	    else {
	        api_msg_handle_cb(fd);
	    }
	}
    }

    return;
}

int dht_thread_connect_to_main (int *sockfd) {
    int dclient_fd             = 0;
    struct sockaddr_un dclient = {0};

    if (sockfd == NULL) {
        printf("\n%s : Fd is NULL", __func__);
        return -1;
    }

    *sockfd = -1;

    if (-1 == (dclient_fd = socket(AF_UNIX, SOCK_SEQPACKET, 0))) {
        printf("\n%s : Socket creation error", __func__);
        return -1;
    }

    memset(&dclient, 0, sizeof(struct sockaddr_un));

    dclient.sun_family = AF_UNIX;
    memset(dclient.sun_path, 0, MAX_SOCK_PATH_LEN);
    strncpy(dclient.sun_path+1, gdata.server_path, MAX_SOCK_PATH_LEN);

    if(-1 == (connect (dclient_fd, (const struct sockaddr *) &dclient, sizeof(struct sockaddr_un)))) {
        printf("\n%s : Socket connect failed, server is down", __func__);
        return -1;
    }

    fcntl(dclient_fd, F_SETFL, (O_NONBLOCK | O_CLOEXEC));

    *sockfd = dclient_fd;

    return 0;
}

int api_thread_server_init (int *sockfd) {
    int opt                   = 1;
    int server_fd             = -1;
    struct sockaddr_in server = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        printf("\n%s: Socket creation failed %d", __func__, errno);
        return -1;
    }
    memset(&server, 0, sizeof(struct sockaddr_in));

    if(setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        printf("\n%s: setsockopt failure",__func__);
        return -1;
    }

    server.sin_family      = AF_INET;
    memcpy(&server.sin_addr, &gdata.api_node.ipv4, sizeof(struct sockaddr_in));
    server.sin_port        = htons(gdata.api_node.port_number);

    if (0 != bind(server_fd, (struct sockaddr *)&server, sizeof(server))) {
        printf("\n%s: Socket bind failed",__func__);
        return -1;
    }

    if (0 != listen(server_fd, MAX_CLIENTS)) {
        printf("\n%s: Listen failed",__func__);
        return -1;
    }

    *sockfd = server_fd;
    return 0;
}

void *dht_start_api_thread(void *data) {
    int dht_server_fd = 0;
    int dht_client_fd = 0;

    dht_api_data_t *api_data = (dht_api_data_t *)data;
    if (!data){
        printf("\n%s: Input data is NULL",__func__);
        return NULL;
    }
    /* Init datamodel */
    gdata.local_client_fd = -1;
    gdata.server_fd = -1;
    memcpy((void*) &gdata.api_node, &api_data->api_node, sizeof(dht_node_data_t));
    strcpy(gdata.server_path, api_data->path);
    free(api_data);

    /* Init datamodel and array list */
    gdata.req_list         = create_list();
    gdata.poll_handle_list = create_list();

    if (0 != dht_thread_connect_to_main(&dht_client_fd)) {
        printf("\n%s :  Failed creating socket for dht server thread",__func__);
        free(gdata.req_list);
        return NULL;
    }
    gdata.local_client_fd = dht_client_fd;

    if (0 != api_thread_server_init(&dht_server_fd)){
        printf("\n%s :  Failed creating socket for dht client thread",__func__);
        close(dht_client_fd);
        free(gdata.req_list);
        return NULL;
    }
    gdata.server_fd = dht_server_fd;

    /* Start Poll */
    gdata.loop = (uv_loop_t *) calloc (1, sizeof(uv_loop_t));

    if (!gdata.loop) {
        printf("\n%s: Unable to allocate memory for uv loop",__func__);
        close(dht_client_fd);
        close(dht_server_fd);
        free(gdata.req_list);
        return NULL;
    }
    uv_loop_init(gdata.loop);

    uv_timer_init  (gdata.loop, &gdata.periodic_timer);
    uv_timer_start (&gdata.periodic_timer, periodic_timer_cb, 1000, 1000);

    uv_poll_init   (gdata.loop, &gdata.local_client_handle, gdata.local_client_fd);
    uv_poll_start  (&gdata.local_client_handle, (UV_READABLE|UV_DISCONNECT), uvpoll_read_cb);

    uv_poll_init   (gdata.loop, &gdata.server_handle, gdata.server_fd);
    uv_poll_start  (&gdata.server_handle, (UV_READABLE|UV_DISCONNECT), uvpoll_read_cb);

    uv_run(gdata.loop, UV_RUN_DEFAULT);

    /* Close loop */
    uv_loop_close(gdata.loop);
    free(gdata.loop);

    return NULL;
}

int dht_init_api_thread(dht_node_data_t data, const char *path) {
    pthread_t api_thread = {0};

    dht_api_data_t *tdata = (dht_api_data_t *) calloc (1, sizeof(dht_api_data_t));
    if (NULL == tdata) {
	printf("\n%s: Calloc failed",__func__);
	return -1;
    }
    memcpy((void *)&tdata->api_node, (void *)&data, sizeof(dht_node_data_t));
    strcpy(tdata->path, path);

    if (0 != pthread_create(&api_thread, NULL,dht_start_api_thread,(void*)tdata)) {
        printf ("\n%s: pthread create failed for api thread",__func__);
        return -1;
    }
    return 0;
}
