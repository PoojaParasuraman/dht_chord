#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dht_list.h"

p2p_list_t * create_list () {
    p2p_list_t *plist = calloc(1, sizeof(p2p_list_t));
    return plist;
}

p2p_node_t *create_node (void *data) {
    p2p_node_t* node = NULL;

    if(data) {
        node = calloc(1, sizeof(p2p_node_t));
        if(node) {
            node->data = data;
        }
    }
    return node;
}

/****************************************************************/
/****************** LOOKUP FUNCTIONS ****************************/
/****************************************************************/

void *peek_first_data(p2p_list_t *plist) {
    void *data = NULL;

    if (plist && plist->head) {
        data = plist->head->data;
    }
    return data;
}

void *peek_last_data(p2p_list_t *plist) {
    void *data = NULL;

    if(plist && plist->tail) {
        data = plist->tail->data;
    }
    return data;
}

static inline p2p_node_t *peek_node_at_index(p2p_list_t* plist, int position) {
    p2p_node_t *node = NULL;

    if(plist && (position < plist->count)) {
        node = plist->head;

        while (((position - 1) >= 0) && node != NULL) {
            node = node->next;
            position--;
        }
    }
    return node;
}

void* find_object (p2p_list_t * list, void* find_obj, int (*is_equal)(void* obj, void* find_obj)) {
    void *node_obj = NULL;

    if(list && is_equal && find_obj) {
        if(list->head != NULL) {
            p2p_node_t *node = list->head;
            while(node) {
                if(is_equal(node->data , find_obj)) {
                    node_obj = node->data;
                    break;
                }
                node = node->next;
            }
        }
    }
    return node_obj;
}


static inline p2p_node_t *node_at_index (p2p_list_t *plist, int position) {
    p2p_node_t *i = NULL;
    if((plist) && (position < plist->count)) {
        i = plist->head;
        while (((position-1) >= 0) && i != NULL) {
            i = i->next; position--;
        }
    }
    return i;
}

void * object_at_index(p2p_list_t* plist, int position) {
    p2p_node_t* node =  NULL;

    node = node_at_index(plist, position);
    void * obj = NULL;
    if(node) {
        obj = node->data;
    }
    return obj;
}

/****************************************************************/
/****************** INSERT FUNCTIONS ****************************/
/****************************************************************/

int insert_first (p2p_list_t *plist, void *data) {
    int ret = -1;

    if(plist && data) {
        p2p_node_t * node = create_node(data);
        if(node) {
            if(plist->head == NULL) {
                plist->head = node;
                plist->tail = plist->head;
            }
            else {
                p2p_node_t* prev_head = plist->head;
                plist->head = node;
                node->next = prev_head;
                prev_head->prev = node;
            }
            plist->count++;
            ret = 0;
        }
    }
    return ret;
}

int insert_last (p2p_list_t *plist, void *data) {
    int ret = -1;

    if(plist && data) {
        p2p_node_t * node = create_node(data);
        if(node) {
            if(plist->head == NULL) {
                plist->head = node;
                plist->tail = plist->head;
            }
            else {
                node->prev = plist->tail;
                plist->tail->next = node;
                plist->tail = node;
            }
            plist->count++;
            ret = 0;
	}
    }
    return ret;
}

int insert_at_position (p2p_list_t *plist, void *data, int index) {
    int ret = -1;

    if(plist && data) {

	if(index == 0) {
            ret = insert_first(plist, data);
        }
        else if(index >= plist->count) {
            ret = insert_last(plist, data);
        }
        else {
            p2p_node_t * prev_node = peek_node_at_index(plist, index);
            if(prev_node) {
                p2p_node_t * node = create_node(data);

		if (node) {
                    node->next = prev_node;
                    node->prev = prev_node->prev;
                    prev_node->prev->next = node;
                    prev_node->prev = node;

		    plist->count++;
                    ret = 0;
		}
            }
        }
    }
    return ret;
}

/**********************************************************************/
/********************* REMOVE FUNCTIONS *******************************/
/**********************************************************************/

void *remove_first (p2p_list_t *plist) {
    void* data = NULL;

    if (plist && plist->head) {
        p2p_node_t *node = plist->head;
	
        if(node == plist->tail) {
            plist->tail = NULL;
        }
        if(node->next) {
            node->next->prev = NULL;
        }

	plist->head = node->next;
        plist->count--;
        data = node->data;
        free(node);
    }

    return data;
}

static inline void remove_node (p2p_list_t* plist, p2p_node_t* node) {
    if(node == plist->head) {
        if(node == plist->tail)
            plist->tail      = NULL;
        if(node->next) {
            node->next->prev = NULL;
        }
        plist->head = node->next;
    }else if(node == plist->tail) {
        plist->tail          = node->prev;
        plist->tail->next    = NULL;
    }else {
        node->prev->next     = node->next;
        node->next->prev     = node->prev;
    }
    plist->count--;
    free(node->data);
    free(node);
}


void *compare_and_remove (p2p_list_t* plist, void *data, int compare(void *node_data, void *data)) {
    if (plist && data) {
        if(plist->head != NULL) {
            p2p_node_t *node      = plist->head;
            p2p_node_t *next_node = node->next;
	    while (node) {
                next_node = node->next;
                if (node->data && (1 == compare(node->data, data))) {
		    remove_node (plist, node);
	        }
	        node = next_node;
	    }
	}
    }
    return NULL;
}

void *remove_last (p2p_list_t* plist) {
    void *data = NULL;

    if(plist) {
        if(plist->head != NULL) {
            if(plist->head == plist->tail) {
                data = plist->head->data;
		free(plist->head);
		plist->head = NULL;
	        plist->tail = NULL;
                plist->count = 0;
            }
            else {
                p2p_node_t *node = plist->tail;

		if (node) {
                    data = plist->tail->data;
                    plist->tail       = node->prev;
                    plist->tail->next = NULL;
                    plist->count--;

                    free(node);
		}
            }
        }
    }
    return data;
}

/********** Parser ****************/
void attach_list_parser (p2p_list_parser_t *p, p2p_list_t *plist) {
    if(p && plist) {
        p->list = plist;
        p->node = plist->head;
    }
}

p2p_list_parser_t *new_list_parser (p2p_list_t *plist) {
    p2p_list_parser_t* p = NULL;
    if(plist) {
        p = (p2p_list_parser_t*) calloc (1,sizeof(p2p_list_parser_t));
        if(p) {
            attach_list_parser(p, plist);
        }
    }
    return p;
}

void free_list_iterator(p2p_list_parser_t *p) {
    free(p);
}

void* get_next_object (p2p_list_parser_t *p) {
    void *obj = NULL;
    if(p && p->node) {
	p2p_node_t *node = p->node;
        obj     = node->data;
        p->node = node->next;
    }
    return obj;
}
