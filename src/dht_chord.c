#include "dht_chord.h"
#include "dht_list.h"
#include "dht_datamodel.h"
#include "dht_utils.h"
#include "finger_table.h"

#include <uv.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

chord_data_t g_chord_data = {0};

int chord_process_notify_successor(chord_msg_t *recv_msg);
int chord_process_ping_predecessor(chord_msg_t *recv_msg);
int chord_process_successor_request(chord_msg_t *recv_msg);
int chord_process_successor_response(chord_msg_t *recv_msg);
int chord_process_predecessor_request(chord_msg_t *recv_msg);
int chord_process_predecessor_response(chord_msg_t *recv_msg);
int chord_process_put_request(chord_msg_t *recv_msg);
int chord_process_get_request(chord_msg_t *recv_msg);
int chord_process_get_response(chord_msg_t *recv_msg);
int chord_process_ack(chord_msg_t *recv_msg);

void chord_join_network ();
int chord_send_ping_predecessor (chord_node_data_t node);
int chord_send_notify_successor(chord_node_data_t node);
int chord_send_successor_request (chord_node_data_t node, uint64_t req_key, int forward, chord_node_data_t src);
int chord_send_successor_response (chord_node_data_t succ, chord_node_data_t node, uint64_t req_key);
int chord_send_predecessor_request (chord_node_data_t node);
int chord_send_predecessor_response (chord_node_data_t node, chord_node_data_t pred);
int chord_send_put_request (chord_node_data_t node, chord_put_request_t *data);
int chord_send_get_request (chord_node_data_t node, uint8_t *key, uint8_t forward, chord_node_data_t src);
int chord_send_get_response (chord_node_data_t node, chord_get_response_t *data);
int chord_send_ack (uint8_t code, chord_node_data_t node);

int chord_send (int fd, void *buf, int size);
void chord_msg_data_free(chord_msg_t *msg);
int dht_thread_connect_to_main (int *sockfd);
int chord_thread_server_init (int *sockfd);
int is_current_stabilise_entry ();

chord_msg_cb_obj_t chord_msg_cb_obj[] = {
    {
        .type      = CHORD_NOTIFY_SUCCESSOR,
        .action_cb = chord_process_notify_successor,
    },
    {
        .type      = CHORD_PING_PREDECESSOR,
        .action_cb = chord_process_ping_predecessor,
    },
    {
        .type      = CHORD_SUCCESSOR_REQUEST,
        .action_cb = chord_process_successor_request,
    },
    {
        .type      = CHORD_SUCCESSOR_RESPONSE,
        .action_cb = chord_process_successor_response,
    },
    {
        .type      = CHORD_PREDECESSOR_REQUEST,
        .action_cb = chord_process_predecessor_request,
    },
    {
        .type      = CHORD_PREDECESSOR_RESPONSE,
        .action_cb = chord_process_predecessor_response,
    },
    {
        .type      = CHORD_PUT_REQUEST,
        .action_cb = chord_process_put_request,
    },
    {
        .type      = CHORD_GET_REQUEST,
        .action_cb = chord_process_get_request,
    },
    {
        .type      = CHORD_GET_RESPONSE,
        .action_cb = chord_process_get_response,
    },
    {
        .type      = CHORD_ACK,
        .action_cb = chord_process_ack,
    },
};

/******************************* Helper Functions ****************************************/

int compare_fd_obj (void *node_data, void *data) {
    int status = 0;
    chord_node_data_t *node = (chord_node_data_t *) data;
    dht_fd_obj_t      *obj  = (dht_fd_obj_t *) node_data;

    if (node && obj) {
        if (0 == memcmp(node, &obj->node, sizeof(chord_node_data_t))) {
	    status = 1;
	}
    }
    return status;
}

int get_node_fd (chord_node_data_t node) {
    int fd                    = -1;
    struct sockaddr_in server = {0};

    dht_fd_obj_t *obj = find_object (g_chord_data.fd_list, (void *) &node, compare_fd_obj);

    if (obj) {
        fd = obj->fd;
    }
    else {
        fd = socket(AF_INET, SOCK_STREAM, 0);
        if (fd == -1) {
            printf("\n%s: socket creation failed", __func__);
            return -1;
        }

        bzero(&server, sizeof(server));
        server.sin_family = AF_INET;
        server.sin_port   = htons(node.port);
        memcpy (&server.sin_addr.s_addr, &node.ipv4.s_addr, sizeof(node.ipv4.s_addr));

        if (connect(fd, (struct sockaddr *)&server, sizeof(server)) != 0) {
            printf("\n%s : connection with the peer failed %d", __func__,errno);
            close (fd);
            return -1;
        }
    
        /* Add to list */
	obj = (dht_fd_obj_t *) calloc (1, sizeof(dht_fd_obj_t));
	obj->fd = fd;
	memcpy(&obj->node, &node, sizeof(chord_node_data_t));

	if (-1 == insert_last(g_chord_data.fd_list, (void *)obj)) {
            printf("\n%s: Insert node at last failed",__func__);
	    close(fd);
            return -1;
        }
    }

    return fd;
}

int chord_send_to_peer (chord_node_data_t node, void *buf, int len) {
    int fd                    = -1;

    if (0 == memcmp (&node, &g_chord_data.chord_node, sizeof(chord_node_data_t))) {
        printf("\n%s: Trying to send to own node\n",__func__);
	return -1;
    }

    fd = get_node_fd(node);

    if (-1 == fd) {
	return -1;
    }

    if (-1 == chord_send (fd, buf, len)) {
        close(fd);
	/* Remove node and connect again and retry */
	compare_and_remove(g_chord_data.fd_list, (void *) &node, compare_fd_obj);
	
	fd = get_node_fd(node);

	if (-1 == fd) {
            return -1;
	}
	
	if (-1 == chord_send (fd, buf, len)) {
	    close(fd);
	    compare_and_remove(g_chord_data.fd_list, (void *) &node, compare_fd_obj);
            printf("\n%s : Chord Send failed", __func__);
	    return -1;
	}
    }

    return 0;
}

int chord_send (int fd, void *buf, int size) {
    if (-1 != fd) {
        int len = 0;
        if (-1 == (len = send(fd, buf, size, 0))){
            printf("\n%s: send failed", __func__);
            return -1;
        }
	if (len != size) {
            printf("\n%s: Mismatch of bytes sent : %d,%d\n",__func__,size,len);
	    return -1;
	}
        return 0;
    }
    return -1;
}

void chord_msg_data_free(chord_msg_t *msg) {
    if (msg && msg->data) {
        free(msg->data);
    }
    return;
}

int compare_chord_req_node (void *a, void *b) {
    if (a && b) {
        chord_req_node_t *find_obj = (chord_req_node_t *)a;
        chord_req_node_t *obj      = (chord_req_node_t *)b;

        if ((0 == memcmp(&find_obj->succ, &obj->succ, sizeof(chord_node_data_t))) &&
            (0 == memcmp(find_obj->put_data.key, obj->put_data.key, MAX_KEY_LEN)))
            return 1;
    }
    return 0;
}

int compare_node_entry (void *node_data, void *data) {
    chord_req_node_t  *ndata = (chord_req_node_t *) node_data;
    chord_node_data_t *cdata = (chord_node_data_t *) data;

    if (ndata && cdata) {
        if (0 == (memcmp(&ndata->succ, cdata, sizeof(chord_node_data_t)))) {
            return 1;
        }
    }
    return 0;
}

void chord_fix_fingers () {
    chord_node_data_t node    = {0};
    uint64_t         key      = 0;
    struct timespec curr_time = get_current_time();

    if (MAX_FT_TIMEOUT > get_clock_diff_secs(curr_time, g_chord_data.stabilise_time))
        return;

    //dump_finger_table();
    switch (g_chord_data.stabilise_status) {
        case FT_COMPLETE: {

            if (is_only_node()) {
		break;
	    }
            else if (is_found_only_succ()) {
                /* Get last successor */
                get_last_succ (&node);

		/* Send get_successor_request for last successor */
                chord_send_successor_request (node, node.key, CHORD_ORIGINAL, node);
		g_chord_data.stabilise_status = FT_SUCC_1;
	    }
            else if (is_only_bootstrapper_present()) {
                get_predecessor(&node);
                chord_send_ping_predecessor(node);
		g_chord_data.stabilise_status = FT_PING_PRED;
		g_chord_data.ack = CHORD_PING_PRED_FAILURE;
	    }
	    else {
		if (is_predecessor_active()) {
                    get_predecessor(&node);
                    chord_send_ping_predecessor(node);
		    g_chord_data.stabilise_status = FT_PING_PRED;
		    g_chord_data.ack = CHORD_PING_PRED_FAILURE;
		    break;
		}
		g_chord_data.stabilise_status = FT_COMPLETE;
	    }
            break;
        }
        case FT_PING_PRED: {
	    if (g_chord_data.ack == CHORD_PING_PRED_FAILURE) {
                printf("\n%s: - No ACK received or got NACK from pred",__func__);
                remove_predecessor();
	    }

            get_node_entry (SUCCESSOR_ID, &node);
            chord_send_predecessor_request(node);
            g_chord_data.stabilise_entry = SUCCESSOR_ID;
	    g_chord_data.stabilise_status = FT_SUCC_0;
	    break;
	}
	case FT_SUCC_0: {
	    if (g_chord_data.stabilise_entry == SUCCESSOR_ID) {
                if (0 == move_and_get_next_succ(&node)) {
                    chord_join_network();
		    break;
		}
	    }
	    else {
                /* Get last successor */
                get_last_succ (&node);

                /* Send get_successor_request for last successor */
                chord_send_successor_request (node, node.key+1, CHORD_ORIGINAL, node);
                g_chord_data.stabilise_status = FT_SUCC_1;
	    }
	    break;
	}
	case FT_SUCC_1: {
	    if (is_only_one_succ()) {
	        g_chord_data.stabilise_status = FT_COMPLETE;
		break;
	    }

	    g_chord_data.stabilise_status = FT_FIND_SUCC;
	    get_node_entry(SUCCESSOR_ID, &node);
            get_finger_key(SUCCESSOR_ID+1, &key);
	    chord_send_successor_request(node, key, CHORD_ORIGINAL, node);
            g_chord_data.stabilise_entry = SUCCESSOR_ID+1;
	    break;
	}
        case FT_FIND_SUCC: {

	    if ((!is_succ_found(g_chord_data.stabilise_entry)) ||
                 (g_chord_data.stabilise_entry >= MAX_FT_SIZE)) {
                g_chord_data.stabilise_status = FT_COMPLETE;
                g_chord_data.stabilise_entry  = SUCCESSOR_ID;
	    }

	    get_node_entry(g_chord_data.stabilise_entry, &node);
            get_finger_key(g_chord_data.stabilise_entry+1, &key);
            chord_send_successor_request(node, key, CHORD_ORIGINAL, node);
            g_chord_data.stabilise_entry++;

            break;
        }
        default : {
	    g_chord_data.stabilise_status = FT_COMPLETE;
            printf("\n%s : Unknown stabilise status",__func__);
            break;
        }
    }

    g_chord_data.stabilise_time = get_current_time();
    return;
}

void chord_periodic_timer_cb (uv_timer_t *handle) {
   if (handle) {
       chord_fix_fingers ();
   }
   return;
}

/******************************************** MAIN Thead Message Handlers *****************************************/

int chord_handle_main_msg (void *buff) {
    dht_notify_data_t *msg = (dht_notify_data_t *) buff;
    if (!msg) {
        printf("\n%s: Msg is NULL", __func__);
	return -1;
    }
    switch(msg->type) {
        case DHT_CHORD_PUT_REQUEST: {
	    chord_node_data_t    succ  = {0};
            chord_put_request_t *pdata = (chord_put_request_t *) msg->data;

            if (!pdata) {
                printf("\n%s: DHT_CHORD_PUT_REQUEST data is NULL\n",__func__);
                return -1;
            }

            /* Find succcessor node */
            uint64_t chord_key = dht_get_hash_key (pdata->key);

	    find_successor(chord_key, &succ);
            /* Send Response */
            chord_send_put_request (succ, pdata);

	    if (pdata->value)
                free(pdata->value);
            free(pdata);

            break;
	}
        case DHT_CHORD_GET_REQUEST: {
            chord_node_data_t succ ={0};
	    uint8_t           *key = (uint8_t *) msg->data;

	    if (!key) {
		printf("\n%s: DHT_CHORD_GET_REQUEST data is NULL",__func__);
		return -1;
	    }
        
	    /* Find succcessor node */
            uint64_t chord_key = dht_get_hash_key (key);
            find_successor(chord_key, &succ);

	    if (0 != memcmp(&succ, &g_chord_data.chord_node, sizeof(chord_node_data_t))) {
                chord_send_get_request (succ, key, CHORD_ORIGINAL, succ);
	    }

	    free(key);
            break;
        }
        case DHT_CHORD_GET_RESPONSE: {
            chord_send_get_req_data_t *resp = (chord_send_get_req_data_t *) msg->data;

            if (!resp) {
                printf("\n%s: DHT_CHORD_GET_RESPONSE data is NULL",__func__);
                return -1;
            }

            chord_send_get_response (resp->node, &resp->data);

	    free(resp);
            break;
        }
	default : {
	    printf("\n%s : Unknown msg type",__func__);
            break;
        }
    }

    return 0;
}

/************************************************** SEND CALLBACKS ************************************************/

void chord_join_network () {
    g_chord_data.stabilise_entry  = SUCCESSOR_ID;
    g_chord_data.stabilise_time   = get_current_time();
    g_chord_data.ack              = CHORD_PING_PRED_FAILURE;
    
    if (0 == memcmp(&g_chord_data.chord_node, &g_chord_data.bootstrap_node, sizeof(chord_node_data_t))) {
	g_chord_data.stabilise_status = FT_COMPLETE;
    }
    else {
        /* Send successor request to bootstrapper */
        chord_send_successor_request (g_chord_data.bootstrap_node, g_chord_data.chord_node.key, CHORD_ORIGINAL, g_chord_data.chord_node);
        g_chord_data.stabilise_status = FT_SUCC_0;
    }

    return;
}

int chord_send_ping_predecessor (chord_node_data_t node) {
    uint8_t *msg,*p       = NULL;
    uint16_t len          = 0;
    uint16_t type         = 0;
    uint8_t reserved      = 0;
    chord_node_data_t lnode = {0};

    type  = CHORD_PING_PREDECESSOR;
    len   = MAX_CHORD_HEADER_LEN;

    msg = p = (uint8_t *) calloc (1, len);
    if (!msg) {
        printf("\n%s : Calloc failure for msg",__func__);
        return -1;
    }

    lnode = g_chord_data.chord_node;

    Put2B(&len,  &p);
    Put2B(&type, &p);
    Put2B(&lnode.port, &p);
    Put1B(&lnode.protocol, &p);
    Put1B(&reserved, &p);
    Put4B((uint32_t *)&lnode.ipv4.s_addr, &p);

    if (-1 == chord_send_to_peer (node, (void *)msg, len)) {
        printf("\n%s : Send failed for CHORD_PING_PREDECESSOR msg", __func__);
    }

    free(msg);
    return 0;

}

int chord_send_notify_successor(chord_node_data_t node) {
    uint8_t *msg,*p       = NULL;
    uint16_t len          = 0;
    uint16_t type         = 0;
    uint8_t reserved      = 0;
    chord_node_data_t lnode = {0};

    type  = CHORD_NOTIFY_SUCCESSOR;
    len   = MAX_CHORD_HEADER_LEN;

    msg = p = (uint8_t *) calloc (1, len);
    if (!msg) {
        printf("\n%s : Calloc failure for msg",__func__);
        return -1;
    }

    lnode = g_chord_data.chord_node;

    Put2B(&len,  &p);
    Put2B(&type, &p);
    Put2B(&lnode.port, &p);
    Put1B(&lnode.protocol, &p);
    Put1B(&reserved, &p);
    Put4B((uint32_t *)&lnode.ipv4.s_addr, &p);

    if (-1 == chord_send_to_peer (node, (void *)msg, len)) {
        printf("\n%s : Send failed for CHORD_NOTIFY_SUCCESSOR msg", __func__);
    }

    free(msg);
    return 0;
}

int chord_send_successor_request (chord_node_data_t node, uint64_t req_key, int forward, chord_node_data_t src) {
    uint8_t *msg,*p       = NULL;
    uint16_t len          = 0;
    uint16_t type         = 0;
    uint8_t reserved      = 0;
    chord_node_data_t lnode = {0};

    type  = CHORD_SUCCESSOR_REQUEST;
    len   = MAX_CHORD_HEADER_LEN + MAX_CHORD_KEY_LEN;

    msg = p = (uint8_t *) calloc (1, len);
    if (!msg) {
        printf("\n%s : Calloc failure for msg",__func__);
        return -1;
    }

    if (forward == CHORD_FORWARD) {
        lnode = src;
    }
    else {
        lnode = g_chord_data.chord_node;
    }

    Put2B(&len,  &p);
    Put2B(&type, &p);
    Put2B(&lnode.port, &p);
    Put1B(&lnode.protocol, &p);
    Put1B(&reserved, &p);
    Put4B((uint32_t *)&lnode.ipv4.s_addr, &p);
    PutnB(&req_key, &p, MAX_CHORD_KEY_LEN);

    if (-1 == chord_send_to_peer (node, (void *)msg, len)) {
        printf("\n%s : Send failed for CHORD_SUCCESSOR_REQUEST msg", __func__);
    }

    free(msg);
    return 0;
}

int chord_send_successor_response (chord_node_data_t succ, chord_node_data_t node, uint64_t req_key) {
    uint8_t *msg,*p       = NULL;
    uint16_t len          = 0;
    uint16_t type         = 0;
    uint8_t reserved      = 0;
    chord_node_data_t lnode = {0};

    len   = MAX_CHORD_HEADER_LEN + MAX_CHORD_SUCC_RESP_BODY_LEN;

    msg = p = (uint8_t *) calloc (1, len);
    if (!msg) {
        printf("\n%s : Calloc failure for msg",__func__);
        return -1;
    }

    type  = CHORD_SUCCESSOR_RESPONSE;
    lnode = g_chord_data.chord_node;

    Put2B(&len,  &p);
    Put2B(&type, &p);
    Put2B(&lnode.port, &p);
    Put1B(&lnode.protocol, &p);
    Put1B(&reserved, &p);
    Put4B((uint32_t *)&lnode.ipv4.s_addr, &p);
    PutnB(&req_key, &p, MAX_CHORD_KEY_LEN);
    PutnB(&succ.key, &p, MAX_CHORD_KEY_LEN);
    Put2B(&succ.port, &p);
    Put1B(&succ.protocol, &p);
    Put1B(&reserved, &p);
    Put4B((uint32_t *)&succ.ipv4.s_addr, &p);

    if (-1 == chord_send_to_peer (node, (void *)msg, len)) {
        printf("\n%s : Send failed for CHORD_SUCCESSOR_RESPONSE msg", __func__);
    }

    free(msg);
    return 0;
}

int chord_send_predecessor_request (chord_node_data_t node) {
    uint8_t *msg,*p       = NULL;
    uint16_t len          = 0;
    uint16_t type         = 0;
    uint8_t reserved      = 0;
    chord_node_data_t lnode = {0};

    len   = MAX_CHORD_HEADER_LEN;

    msg = p = (uint8_t *) calloc (1, len);
    if (!msg) {
        printf("\n%s : Calloc failure for msg",__func__);
        return -1;
    }

    type  = CHORD_PREDECESSOR_REQUEST;
    lnode = g_chord_data.chord_node;

    Put2B(&len,  &p);
    Put2B(&type, &p);
    Put2B(&lnode.port, &p);
    Put1B(&lnode.protocol, &p);
    Put1B(&reserved, &p);
    Put4B((uint32_t *)&lnode.ipv4.s_addr, &p);

    if (-1 == chord_send_to_peer (node, (void *)msg, len)) {
        printf("\n%s : Send failed for CHORD_PREDECESSOR_REQUEST msg", __func__);
    }

    free(msg);
    return 0;
}

int chord_send_predecessor_response (chord_node_data_t node, chord_node_data_t pred) {
    uint8_t *msg,*p       = NULL;
    uint16_t len          = 0;
    uint16_t type         = 0;
    uint8_t reserved      = 0;
    chord_node_data_t lnode = {0};

    len   = MAX_CHORD_HEADER_LEN + MAX_PRED_RESP_BODY_LEN;

    msg = p = (uint8_t *) calloc (1, len);
    if (!msg) {
        printf("\n%s : Calloc failure for msg",__func__);
        return -1;
    }

    type  = CHORD_PREDECESSOR_RESPONSE;
    lnode = g_chord_data.chord_node;

    Put2B(&len,  &p);
    Put2B(&type, &p);
    Put2B(&lnode.port, &p);
    Put1B(&lnode.protocol, &p);
    Put1B(&reserved, &p);
    Put4B((uint32_t *)&lnode.ipv4.s_addr, &p);
    Put2B(&pred.port, &p);
    Put1B(&pred.protocol, &p);
    Put1B(&reserved, &p);
    Put4B((uint32_t *)&pred.ipv4.s_addr, &p);

    if (-1 == chord_send_to_peer (node, (void *)msg, len)) {
        printf("\n%s : Send failed for CHORD_PREDECESSOR_RESPONSE msg", __func__);
    }

    free(msg);
    return 0;
}

int chord_send_put_request (chord_node_data_t node, chord_put_request_t *data) {
    uint8_t *msg,*p       = NULL;
    uint16_t len          = 0;
    uint16_t type         = 0;
    uint8_t reserved      = 0;
    chord_node_data_t lnode = {0};

    if (!data || (data->value_len <= 0) || !(data->value)) {
        printf("\n%s : chord_put_request_t is NULL", __func__);
        return -1;
    }

    len   = MAX_CHORD_HEADER_LEN + MIN_CHORD_PUT_REQ_BODY_LEN + data->value_len;

    msg = p = (uint8_t *) calloc (1, len);
    if (!msg) {
        printf("\n%s : Calloc failure for msg",__func__);
        return -1;
    }

    type  = CHORD_PUT_REQUEST;
    lnode = g_chord_data.chord_node;

    Put2B(&len,  &p);
    Put2B(&type, &p);
    Put2B(&lnode.port, &p);
    Put1B(&lnode.protocol, &p);
    Put1B(&reserved, &p);
    Put4B((uint32_t *)&lnode.ipv4.s_addr, &p);
    PutnB(data->key, &p, MAX_KEY_LEN);
    Put2B(&data->ttl, &p);
    Put1B(&data->replica, &p);
    Put1B(&reserved, &p);
    PutnB(data->value, &p, data->value_len);

    if (-1 == chord_send_to_peer (node, (void *)msg, len)) {
        printf("\n%s : Send failed for CHORD ACK msg", __func__);
    }

    free(msg);
    return 0;
}

int chord_send_get_request (chord_node_data_t node, uint8_t *key, uint8_t forward, chord_node_data_t src) {
    uint8_t *msg,*p       = NULL;
    uint16_t len          = 0;
    uint16_t type         = 0;
    uint8_t reserved      = 0;
    chord_node_data_t lnode = {0};

    if (!key) {
        printf("\n%s : get_request key is NULL", __func__);
        return -1;
    }

    len   = MAX_CHORD_HEADER_LEN + MAX_KEY_LEN;

    msg = p = (uint8_t *) calloc (1, len);
    if (!msg) {
        printf("\n%s : Calloc failure for msg",__func__);
        return -1;
    }

    type  = CHORD_GET_REQUEST;
    if (forward == CHORD_FORWARD) {
        lnode = src;
    }
    else {
        lnode = g_chord_data.chord_node;
    }

    Put2B(&len,  &p);
    Put2B(&type, &p);
    Put2B(&lnode.port, &p);
    Put1B(&lnode.protocol, &p);
    Put1B(&reserved, &p);
    Put4B((uint32_t *)&lnode.ipv4.s_addr, &p);
    PutnB(key, &p, MAX_KEY_LEN);

    if (-1 == chord_send_to_peer (node, (void *)msg, len)) {
        printf("\n%s : Send failed for CHORD ACK msg", __func__);
    }

    free(msg);
    return 0;
}

int chord_send_get_response (chord_node_data_t node, chord_get_response_t *data) {
    uint8_t *msg,*p       = NULL;
    uint16_t len          = 0;
    uint16_t type         = 0;
    uint8_t reserved      = 0;
    chord_node_data_t lnode = {0};

    if (!data || (data->value_len <= 0)) {
        printf("\n%s : chord_get_response_t is NULL", __func__);
        return -1;
    }

    len   = MAX_CHORD_HEADER_LEN + MAX_KEY_LEN + data->value_len;

    msg = p = (uint8_t *) calloc (1, len);
    if (!msg) {
        printf("\n%s : Calloc failure for msg",__func__);
        return -1;
    }

    type  = CHORD_GET_RESPONSE;
    lnode = g_chord_data.chord_node;

    Put2B(&len,  &p);
    Put2B(&type, &p);
    Put2B(&lnode.port, &p);
    Put1B(&lnode.protocol, &p);
    Put1B(&reserved, &p);
    Put4B((uint32_t *)&lnode.ipv4.s_addr, &p);
    PutnB(data->key, &p, MAX_KEY_LEN);
    PutnB(data->value, &p, data->value_len);

    if (-1 == chord_send_to_peer (node, (void *)msg, len)) {
        printf("\n%s : Send failed for CHORD ACK msg", __func__);
    }

    free(msg);
    return 0;
}

int chord_send_ack (uint8_t code, chord_node_data_t node) {
    uint8_t *msg,*p       = NULL;
    uint16_t len          = 0;
    uint16_t type         = 0;
    uint8_t reserved      = 0;
    chord_node_data_t lnode = {0};

    len   = MAX_CHORD_HEADER_LEN + 1;

    msg = p = (uint8_t *) calloc (1, len);
    if (!msg) {
        printf("\n%s : Calloc failure for msg",__func__);
        return -1;
    }

    type  = CHORD_ACK;
    lnode = g_chord_data.chord_node;

    Put2B(&len,  &p);
    Put2B(&type, &p);
    Put2B(&lnode.port, &p);
    Put1B(&lnode.protocol, &p);
    Put1B(&reserved, &p);
    Put4B((uint32_t *)&lnode.ipv4.s_addr, &p);
    Put1B(&code, &p);

    if (-1 == chord_send_to_peer (node, (void *)msg, len)) {
        printf("\n%s : Send failed for CHORD ACK msg", __func__);
    }

    free(msg);
    return 0;
}

/**************************************** MSG HANDLERS ***************************************/

int chord_process_notify_successor(chord_msg_t *recv_msg) {
    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    /* store predecessor node */
    if (!is_my_pred(&recv_msg->node)) {
        update_predecessor(&recv_msg->node);

        /* Send Share Data request to main */
        uint64_t *key = (uint64_t *) calloc (1, sizeof(uint64_t));

        *key = recv_msg->node.key;
        if (-1 == dht_sendto_thread (key, DHT_CHORD_SHARE_DATA, g_chord_data.local_client_fd)) {
            free(key);
        }

	if (is_only_node()) {
	    /* store succcessor node */
            update_finger_entry(SUCCESSOR_ID, &recv_msg->node);
            chord_send_notify_successor(recv_msg->node);
	    g_chord_data.stabilise_status = FT_SUCC_1;
	}
    }

    return 0;
}

int chord_process_ping_predecessor(chord_msg_t *recv_msg) {
    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    /* Send (N)ACK */
    if (is_my_successor (&recv_msg->node)) {
        chord_send_ack(CHORD_PING_PRED_SUCCESS, recv_msg->node);

        /* store succcessor node */
        update_finger_entry(SUCCESSOR_ID, &recv_msg->node);
    }
    else {
       chord_send_ack(CHORD_PING_PRED_FAILURE, recv_msg->node);
    }

    return 0;
}

int chord_process_successor_request(chord_msg_t *recv_msg) {
    uint64_t *key          = NULL;
    chord_node_data_t succ = {0};

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    key = (uint64_t *) recv_msg->data;
    if (!key) {
        printf("\n%s : Get data is NULL",__func__);
        return -1;
    }

    get_node_entry(SUCCESSOR_ID, &succ);

    if (is_local_node_successor(*key) || is_only_node() || (*key == succ.key)) {
	/* Send Response */
	chord_send_successor_response (g_chord_data.chord_node, recv_msg->node, *key);
    }
    else {
        /* Find succcessor node */
        find_successor(*key, &succ);
	chord_send_successor_response (succ, recv_msg->node, *key);
    }
    return 0;
}

int chord_process_successor_response(chord_msg_t *recv_msg) {
    chord_successor_resp_t *succ_resp = NULL;
    chord_node_data_t succ            = {0};
    uint64_t          key             = 0;

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    succ_resp = (chord_successor_resp_t *) recv_msg->data;
    if (!succ_resp) {
        printf("\n%s : Get data is NULL",__func__);
        return -1;
    }

    if (g_chord_data.stabilise_status == FT_SUCC_0) {
	update_finger_entry(SUCCESSOR_ID, &succ_resp->data);
        chord_send_notify_successor(succ_resp->data);

        chord_send_successor_request (succ_resp->data, succ_resp->data.key+1, CHORD_ORIGINAL, succ_resp->data);
        g_chord_data.stabilise_status = FT_SUCC_1;
	g_chord_data.stabilise_entry = SUCCESSOR_ID+1;
    }
    else if (g_chord_data.stabilise_status == FT_SUCC_1) {
	get_node_entry(SUCCESSOR_ID, &succ);

	if ((succ.key != succ_resp->data.key) && (succ_resp->data.key != g_chord_data.chord_node.key)){
            update_successor_list(&succ_resp->data);
	    g_chord_data.stabilise_status = FT_FIND_SUCC;
	    g_chord_data.stabilise_entry = SUCCESSOR_ID+1;
	}
    }
    else if (g_chord_data.stabilise_status == FT_FIND_SUCC) {
        get_finger_key(g_chord_data.stabilise_entry, &key);

	if (succ_resp->request_key == key) {
            update_finger_entry(g_chord_data.stabilise_entry, &recv_msg->node);
	}
    }

    return 0;
}

int chord_process_predecessor_request(chord_msg_t *recv_msg) {
    chord_node_data_t pred = {0};

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    if (is_predecessor_active()) {
        get_predecessor(&pred);
        chord_send_predecessor_response (recv_msg->node, pred);
    }

    return 0;
}

int chord_process_predecessor_response(chord_msg_t *recv_msg) {
    chord_node_data_t *succ = NULL;

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    succ = (chord_node_data_t *) recv_msg->data;
    if (!succ) {
        printf("\n%s : Get data is NULL",__func__);
        return -1;
    }

    if (is_my_successor(&recv_msg->node) && (0 != memcmp(&g_chord_data.chord_node,succ, sizeof(chord_node_data_t)))) {
        update_finger_entry (SUCCESSOR_ID, succ);
        chord_send_notify_successor(*succ);
    }
    g_chord_data.stabilise_entry = SUCCESSOR_ID+1;

    return 0;
}

int chord_process_put_request (chord_msg_t *recv_msg) {
    chord_put_request_t *put_data = NULL;
    chord_node_data_t    node     = {0};

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    put_data = (chord_put_request_t *) recv_msg->data;
    if (!put_data) {
        printf("\n%s : Put data is NULL",__func__);
        return -1;
    }

    /* If hop count < MAX_HOP_COUNT and src node = prev node */
    if ((put_data->replica <= MAX_HOP_COUNT) && ((is_my_pred(&recv_msg->node)) || (put_data->replica == MAX_HOP_COUNT))) {

	chord_put_request_t *send_data = (chord_put_request_t *) calloc (1,sizeof(chord_put_request_t));

	if (send_data) {
            memcpy (send_data, put_data, sizeof (chord_put_request_t));

	    send_data->value = (uint8_t *) calloc (1,put_data->value_len);
	    if (!send_data->value) {
		free(send_data);
		free(put_data->value);
		return -1;
	    }

	    memcpy(send_data->value, put_data->value, put_data->value_len);
            /* send to main */
            if (-1 == dht_sendto_thread (send_data, DHT_CHORD_PUT_REQUEST, g_chord_data.local_client_fd)) {
		free(send_data->value);
	        free(send_data);
	    }
	}

        /* Decrement hop count */
        put_data->replica = put_data->replica-1;

        /* if hop count > 0, Forward to next node */
        if (put_data->replica > 0) {
            get_node_entry(SUCCESSOR_ID, &node);
	    if (0 != memcmp(&node, &recv_msg->node, sizeof(chord_node_data_t))) {
                chord_send_put_request(node, put_data);
	    }
        }
    }
    free(put_data->value);
    return 0;
}

int chord_process_get_request (chord_msg_t *recv_msg) {
    uint8_t *key                  = NULL;
    uint64_t chord_key            = 0;
    chord_node_data_t succ        = {0};
    chord_get_req_data_t *get_req = NULL;

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    key = (uint8_t *) recv_msg->data;
    if (!key) {
        printf("\n%s : Get data is NULL",__func__);
        return -1;
    }

    /* Find the hash of key */
    chord_key = dht_get_hash_key (key);
    /* Find succcessor node */
    find_successor(chord_key, &succ);

    if (is_local_node_successor (chord_key) || (0 == memcmp(&recv_msg->node, &succ, sizeof(chord_node_data_t)))) {

        get_req = (chord_get_req_data_t *) calloc (1, sizeof (chord_get_req_data_t));

	if (!get_req) {
	    printf("\n%s: calloc failed for chord_get_req_data_t", __func__);
	    return -1;
	}

	get_req->node = recv_msg->node;
	memcpy (get_req->key, key, MAX_KEY_LEN);
        dht_sendto_thread (get_req, DHT_CHORD_GET_REQUEST, g_chord_data.local_client_fd);
    }
    else {
        /* Forward to the successor of key or to the nearest known predecessor */
        if (chord_key > succ.key) {
            /* Forward the request */
            chord_send_get_request (succ, key, CHORD_FORWARD, recv_msg->node);
        }
    }

    return 0;
}

int chord_process_get_response (chord_msg_t *recv_msg) {
    chord_get_response_t *get_resp = NULL;;

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    get_resp = (chord_get_response_t *) recv_msg->data;
    if (!get_resp) {
        printf("\n%s : Get data is NULL",__func__);
        return -1;
    }

    chord_get_response_t *send_data = (chord_get_response_t *) calloc (1,sizeof(chord_get_response_t));

    if (send_data) {
	memcpy(send_data, get_resp, sizeof(chord_get_response_t));
        /* Send to main */
        if (-1 == dht_sendto_thread (send_data, DHT_CHORD_GET_RESPONSE, g_chord_data.local_client_fd)) {
	    free(send_data);
	    free(get_resp->value);
	}
    }

    return 0;
}

int chord_process_ack (chord_msg_t *recv_msg) {
    uint8_t *code = NULL;

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    code = (uint8_t *) recv_msg->data;
    if (!code) {
        printf("\n%s : Code is NULL",__func__);
        return -1;
    }

    g_chord_data.ack = *code;

    if (*code == CHORD_PING_PRED_FAILURE) {
        remove_predecessor();
    }
    return 0;
}

/***************************************** End of Msg Handlers ***************************************************************/
/*****************************************************************************************************************************/

int chord_parse_msg(int fd, uint8_t *buf, chord_msg_t *msg, int len) {
    uint8_t reserved = 0;

    if (!buf || !msg) {
        printf("\n%s : Invalid input",__func__);
        return -1;
    }

    Get2B(&buf, &msg->len);
    Get2B(&buf, &msg->type);

    if (msg->len != len) {
        printf("\n%s : Length Mismatch for type",__func__);
        return -1;
    }
    Get2B(&buf, &msg->node.port);
    Get1B(&buf, &msg->node.protocol);
    Get1B(&buf, &reserved);
    Get4B(&buf, (uint32_t *) &msg->node.ipv4.s_addr);
    msg->fd = fd;

    msg->node.key = dht_get_node_key(msg->node.port, (uint32_t) msg->node.ipv4.s_addr);
    switch(msg->type) {
        case CHORD_PING_PREDECESSOR :
        case CHORD_NOTIFY_SUCCESSOR :
	case CHORD_PREDECESSOR_REQUEST: {
            msg->data = NULL;
            break;
        }
        case CHORD_SUCCESSOR_REQUEST : {
	    if (msg->len != MAX_CHORD_HEADER_LEN + MAX_CHORD_KEY_LEN) {
                printf("\n%s : Invalid length for CHORD_SUCCESSOR_REQUEST message",__func__);
                return -1;
            }
            uint64_t *key = (uint64_t *) calloc (1, sizeof(uint64_t));

	    if (!key) {
                printf("\n%s : calloc failed for key", __func__);
		return -1;
	    }

            GetnB(&buf, key, MAX_CHORD_KEY_LEN);

	    msg->data = (void *) key;
            break;
        }
        case CHORD_SUCCESSOR_RESPONSE : {
            if (msg->len != MAX_CHORD_HEADER_LEN + MAX_CHORD_SUCC_RESP_BODY_LEN) {
                printf("\n%s : Invalid length for CHORD_SUCCESSOR_RESPONSE message",__func__);
                return -1;
            }

            chord_successor_resp_t *resp = (chord_successor_resp_t *) calloc (1, sizeof(chord_successor_resp_t));

	    if (!resp) {
		printf("\n%s : calloc failed for chord_successor_resp_t", __func__);
                return -1;
            }
            GetnB(&buf, &resp->request_key, MAX_CHORD_KEY_LEN);
	    GetnB(&buf, &resp->data.key, MAX_CHORD_KEY_LEN);
	    Get2B(&buf, &resp->data.port);
	    Get1B(&buf, &resp->data.protocol);
	    Get1B(&buf, &reserved);
	    Get4B(&buf, (uint32_t *) &resp->data.ipv4.s_addr);

	    msg->data = (void *) resp;
	    break;
        }
	case CHORD_PREDECESSOR_RESPONSE: {

	    if (msg->len != (MAX_CHORD_HEADER_LEN + MAX_PRED_RESP_BODY_LEN)) {
                printf("\n%s : Invalid length for CHORD_PREDECESSOR_RESPONSE message",__func__);
                return -1;
            }

	    chord_node_data_t *node = (chord_node_data_t *) calloc (1,sizeof(chord_node_data_t));

	    if (!node) {
                printf("\n%s : calloc failed for chord_node_data_t", __func__);
                return -1;
	    }

	    Get2B(&buf, &node->port);
            Get1B(&buf, &node->protocol);
            Get1B(&buf, &reserved);
            Get4B(&buf, (uint32_t *) &node->ipv4.s_addr);
            node->key = dht_get_node_key(node->port, (uint32_t) node->ipv4.s_addr);
	    msg->data = (void *) node;
	    break;
	}
        case CHORD_PUT_REQUEST : {
            if (msg->len <= (MAX_CHORD_HEADER_LEN + MIN_CHORD_PUT_REQ_BODY_LEN)) {
                printf("\n%s : Invalid length for CHORD_PUT_REQUEST message",__func__);
                return -1;
            }

            chord_put_request_t *put_data = (chord_put_request_t *) calloc (1, sizeof(chord_put_request_t));

	    if (!put_data) {
                printf("\n%s : calloc failed for chord_put_request_t", __func__);
                return -1;
            }
            GetnB(&buf, put_data->key, MAX_KEY_LEN);
            Get2B(&buf, &put_data->ttl);
            Get1B(&buf, &put_data->replica);
            Get1B(&buf, &reserved);

	    int value_len = msg->len - (MAX_CHORD_HEADER_LEN + MIN_CHORD_PUT_REQ_BODY_LEN);
	    put_data->value_len = value_len;

            if (value_len == 0) {
                printf("\n%s : data value is 0",__func__);
		free(put_data);
		return -1;
	    }

	    put_data->value = (uint8_t *) calloc (1, value_len * sizeof(uint8_t));
	    if (!put_data->value) {
		printf("\n%s : calloc failed for put_data->value", __func__);
		free(put_data);
                return -1;
            }
            GetnB(&buf, put_data->value, value_len);

            msg->data = (void *) put_data;
            break;
        }
        case CHORD_GET_REQUEST : {
            if (msg->len != MAX_CHORD_HEADER_LEN + MAX_KEY_LEN) {
                printf("\n%s : Invalid length for CHORD_GET_REQUEST message",__func__);
                return -1;
            }

            uint8_t *key = (uint8_t *) calloc (1, MAX_KEY_LEN);

            if (!key) {
                printf("\n%s : calloc failed for key", __func__);
                return -1;
            }

            GetnB(&buf, key, MAX_KEY_LEN);

            msg->data = (void *) key;
            break;
        }
	case CHORD_GET_RESPONSE: {

            if (msg->len  < (MAX_CHORD_HEADER_LEN + MAX_KEY_LEN)) {
                printf("\n%s : Invalid length for DHT_PUT message",__func__);
                return -1;
            }
            chord_get_response_t *get_msg = (chord_get_response_t *) calloc (1, sizeof(chord_get_response_t));

            if (!get_msg) {
                printf("\n%s : Calloc failed for get_msg",__func__);
                return -1;
            }

            GetnB(&buf, get_msg->key, MAX_KEY_LEN);

	    int data_len       = msg->len - (MAX_CHORD_HEADER_LEN + MAX_KEY_LEN);
	    get_msg->value_len = data_len;
            if (data_len == 0) {
                printf("\n%s : data value is 0",__func__);
                free(get_msg);
                return -1;
	    }
	    get_msg->value = (uint8_t *) calloc (1, data_len);
            if (!get_msg->value) {
                printf("\n%s : Calloc failed for get_msg->value",__func__);
	        free(get_msg);
		return -1;
            }
            GetnB(&buf, get_msg->value, data_len);

	    msg->data = (void *) get_msg;
            break;
        }
        case CHORD_ACK: {

            if (msg->len != MAX_CHORD_HEADER_LEN+1) {
                printf("\n%s : Invalid length for CHORD_ACK message",__func__);
                return -1;
            }

	    uint8_t *code = (uint8_t *) calloc (1, sizeof(uint8_t));

            if (!code) {
                printf("\n%s : Calloc failed for code",__func__);
                return -1;
            }

            Get1B(&buf, code);

            msg->data = (void *) code;
            break;
        }
        default: {
            printf("\n%s : Invalid message type - %d",__func__,msg->type);
	    return -1;
        }
    }
    return 0;
}

int chord_get_msg_index(uint16_t type) {
    int index = 0;
    for (index = 0; index < ARRAY_LEN(chord_msg_cb_obj, chord_msg_cb_obj_t); index++) {
       if (chord_msg_cb_obj[index].type == type)
           break;
    }

    if ( index >= ARRAY_LEN(chord_msg_cb_obj, chord_msg_cb_obj_t) ) {
        return -1;
    }
    return index;
}

int chord_handle_message(int fd, uint8_t *buf, int msg_len) {
    chord_msg_cb_obj_t *obj = NULL;
    chord_msg_t msg     = {0};
    int index             = -1;
    int status            = -1;

    do {
        if (-1 == chord_parse_msg(fd, buf, &msg, msg_len)) {
            printf("\n%s : Incorrect msg format, failed to parse", __func__);
            break;
        }

        index = chord_get_msg_index(msg.type);

        if (-1 == index) {
            printf("\n%s : Unknown message type %d", __func__,msg.type);
            break;
        }

        obj = &chord_msg_cb_obj[index];
        if (!obj) {
            printf("\n%s: No object for index %d", __func__, index);
            break;
        }

        if( (obj->action_cb != NULL) && (0 != obj->action_cb(&msg))) {
            printf("Action cb Error for msg_type %d", msg.type);
            break;
        }
        status = 0;
    } while (0);

    chord_msg_data_free(&msg);

    return status;
}

int chord_parse_server_msg (int sockfd) {
    int msg_len            = 0;
    dht_notify_data_t buff = {0};

    if (sockfd != -1) {
        msg_len = recv(sockfd, &buff, sizeof(dht_notify_data_t), 0);

        if (-1 == msg_len) {
            printf("\n%s: read failure",__func__);
            return -1;
	}

	chord_handle_main_msg(&buff);
    }
    return 0;
}

int chord_msg_handle_cb(int sockfd) {
    int      msg_len                     = 0;
    uint8_t  sock_buffer[MAX_PACKET_LEN] = {0};

    if (sockfd != -1) {
        memset (sock_buffer, 0, MAX_PACKET_LEN);

        msg_len = read(sockfd, sock_buffer, MAX_PACKET_LEN);

        if (-1 == msg_len) {
            printf("\n%s Could not read data from socket - %d\n",__func__,errno);
            return -1;
	}

        //sock_buffer[msg_len] = '\0';
        chord_handle_message(sockfd, sock_buffer, msg_len);
    }
    return 0;
}


void chord_uvpoll_read_cb (uv_poll_t* handle, int status, int events)
{
    int fd = -1;
    if (handle) {
	uv_fileno((uv_handle_t *)handle, &fd);
        if((status < 0) || (events & UV_DISCONNECT))
        {
            uv_poll_stop(handle);
            if (events & UV_DISCONNECT) {
	        if (fd == g_chord_data.local_client_fd) {
	            close(g_chord_data.local_client_fd);
                    int ret = dht_thread_connect_to_main(&g_chord_data.local_client_fd);
                    while (ret < 0) {
                        ret = dht_thread_connect_to_main(&g_chord_data.local_client_fd);
                    }
                    uv_poll_init  (g_chord_data.loop, &g_chord_data.local_client_handle, g_chord_data.local_client_fd);
                    uv_poll_start (&g_chord_data.local_client_handle, (UV_READABLE|UV_DISCONNECT), chord_uvpoll_read_cb);
	        }
		else if (fd == g_chord_data.server_fd) {
		    close(g_chord_data.server_fd);
		    int ret = chord_thread_server_init(&g_chord_data.server_fd);
		    while (ret < 0) {
			ret = chord_thread_server_init(&g_chord_data.server_fd);
		    }
                    uv_poll_init  (g_chord_data.loop, &g_chord_data.server_handle, g_chord_data.server_fd);
                    uv_poll_start (&g_chord_data.server_handle, (UV_READABLE|UV_DISCONNECT), chord_uvpoll_read_cb);
		}
		else {
		    compare_and_remove (g_chord_data.poll_handle_list, (void *) handle, compare_handle_entry);
                    close(fd);
		}
	    }
        }
        else if (events & UV_READABLE) {
	    if (fd == g_chord_data.server_fd) {
	        /* Accept connections from client */
                int client_fd = -1;
                if (-1 == (client_fd = accept(g_chord_data.server_fd, NULL,NULL))) {
                    printf("\n%s: accept client connection failed",__func__);
	            return;
                }
	        /* Start uv poll for the client */
		uv_poll_t *handle = (uv_poll_t *) calloc (1, sizeof(uv_poll_t));

                uv_poll_init  (g_chord_data.loop, handle, client_fd);
                uv_poll_start (handle, (UV_READABLE|UV_DISCONNECT), chord_uvpoll_read_cb);

		if (-1 == insert_last(g_chord_data.poll_handle_list, (void *)handle)) {
                    printf("\n%s: Insert node at last failed",__func__);
                    return;
                }
	    }
	    else if (fd == g_chord_data.local_client_fd) {
	        chord_parse_server_msg(fd);
	    }
	    else {
	        chord_msg_handle_cb(fd);
	    }
	}
    }

    return;
}

int dht_chord_thread_connect_to_main (int *sockfd) {
    int dclient_fd             = 0;
    struct sockaddr_un dclient = {0};

    if (sockfd == NULL) {
        printf("\n%s : Fd is NULL", __func__);
        return -1;
    }

    *sockfd = -1;

    if (-1 == (dclient_fd = socket(AF_UNIX, SOCK_SEQPACKET, 0))) {
        printf("\n%s : Socket creation error", __func__);
        return -1;
    }

    memset(&dclient, 0, sizeof(struct sockaddr_un));

    dclient.sun_family = AF_UNIX;
    memset(dclient.sun_path, 0, MAX_SOCK_PATH_LEN);
    strncpy(dclient.sun_path+1, g_chord_data.server_path, MAX_SOCK_PATH_LEN);

    if(-1 == (connect (dclient_fd, (const struct sockaddr *) &dclient, sizeof(struct sockaddr_un)))) {
        printf("\n%s : Socket connect failed, server is down", __func__);
        return -1;
    }

    fcntl(dclient_fd, F_SETFL, (O_NONBLOCK | O_CLOEXEC));

    *sockfd = dclient_fd;

    return 0;
}

int chord_thread_server_init (int *sockfd) {
    int opt                   = 1;
    int server_fd             = -1;
    struct sockaddr_in server = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        printf("\n%s: Socket creation failed %d", __func__,errno);
        return -1;
    }
    memset(&server, 0, sizeof(struct sockaddr_in));

    if(setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        printf("\n %s: setsockopt failure",__func__);
        return -1;
    }

    server.sin_family      = AF_INET;
    memcpy(&server.sin_addr, &g_chord_data.chord_node.ipv4, sizeof(struct sockaddr_in));
    server.sin_port        = htons(g_chord_data.chord_node.port);

    if (0 != bind(server_fd, (struct sockaddr *)&server, sizeof(server))) {
        printf("\n%s: Socket bind failed",__func__);
        return -1;
    }

    if (0 != listen(server_fd, MAX_CHORD_CLIENTS)) {
        printf("\n%s: Listen failed",__func__);
        return -1;
    }

    *sockfd = server_fd;
    return 0;
}

void *dht_start_chord_thread(void *data) {
    int dht_server_fd = 0;
    int dht_client_fd = 0;

    dht_chord_data_t *chord_data = (dht_chord_data_t *)data;
    if (!chord_data){
        printf("\n%s: Input data is NULL",__func__);
        return NULL;
    }

    /* Init datamodel */
    g_chord_data.local_client_fd = -1;
    g_chord_data.server_fd = -1;

    strcpy(g_chord_data.server_path,chord_data->path);

    g_chord_data.chord_node.key      = dht_get_node_key(chord_data->chord_node.port_number, (uint32_t) chord_data->chord_node.ipv4.s_addr);
    g_chord_data.chord_node.port     = chord_data->chord_node.port_number;
    g_chord_data.chord_node.protocol = (uint8_t)chord_data->chord_node.protocol;
    memcpy(&g_chord_data.chord_node.ipv4, &chord_data->chord_node.ipv4, sizeof(struct in_addr));

    g_chord_data.bootstrap_node.key      = dht_get_node_key(chord_data->bootstrap_node.port_number, (uint32_t) chord_data->bootstrap_node.ipv4.s_addr);
    g_chord_data.bootstrap_node.port     = chord_data->bootstrap_node.port_number;
    g_chord_data.bootstrap_node.protocol = (uint8_t)chord_data->bootstrap_node.protocol;
    memcpy(&g_chord_data.bootstrap_node.ipv4, &chord_data->bootstrap_node.ipv4, sizeof(struct in_addr));

    free(chord_data);

    /* Init array list */
    g_chord_data.poll_handle_list = create_list();
    g_chord_data.fd_list          = create_list();

    if (0 != dht_chord_thread_connect_to_main(&dht_client_fd)) {
        printf("\n%s :  Failed creating socket for dht server thread",__func__);
        return NULL;
    }
    g_chord_data.local_client_fd = dht_client_fd;

    if (0 != chord_thread_server_init(&dht_server_fd)){
        printf("\n%s :  Failed creating socket for dht client thread",__func__);
        close(dht_client_fd);
        return NULL;
    }
    g_chord_data.server_fd = dht_server_fd;

    /* Init Finger Table */
    initialise_finger_table(g_chord_data.chord_node.key, g_chord_data.chord_node);

    /* Join Network */
    chord_join_network();

    /* Start Poll */
    g_chord_data.loop = (uv_loop_t *) calloc (1, sizeof(uv_loop_t));

    if (!g_chord_data.loop) {
        printf("\n%s: Unable to allocate memory for uv loop",__func__);
        close(dht_client_fd);
        close(dht_server_fd);
        return NULL;
    }
    uv_loop_init(g_chord_data.loop);

    uv_timer_init  (g_chord_data.loop, &g_chord_data.periodic_timer);
    uv_timer_start (&g_chord_data.periodic_timer, chord_periodic_timer_cb, 1000, 1000);

    uv_poll_init   (g_chord_data.loop, &g_chord_data.local_client_handle, g_chord_data.local_client_fd);
    uv_poll_start  (&g_chord_data.local_client_handle, (UV_READABLE|UV_DISCONNECT), chord_uvpoll_read_cb);

    uv_poll_init   (g_chord_data.loop, &g_chord_data.server_handle, g_chord_data.server_fd);
    uv_poll_start  (&g_chord_data.server_handle, (UV_READABLE|UV_DISCONNECT), chord_uvpoll_read_cb);

    uv_run(g_chord_data.loop, UV_RUN_DEFAULT);

    /* Close loop */
    uv_loop_close(g_chord_data.loop);
    free(g_chord_data.loop);

    return NULL;
}

int dht_init_chord_thread(dht_node_data_t ch, dht_node_data_t bt, const char *path) {
    pthread_t chord_thread = {0};

    dht_chord_data_t *cdata = (dht_chord_data_t *) calloc (1, sizeof(dht_chord_data_t));
    if (NULL == cdata) {
        printf("\n%s: Calloc failed",__func__);
        return -1;
    }
    memcpy((void *)&cdata->chord_node, (void *)&ch, sizeof(dht_node_data_t));
    memcpy((void *)&cdata->bootstrap_node, (void *)&bt, sizeof(dht_node_data_t));
    strcpy(cdata->path, path);

    if (0 != pthread_create(&chord_thread, NULL,dht_start_chord_thread,(void*)cdata)) {
        printf ("\n%s: pthread create failed for chord thread",__func__);
        return -1;
    }
    return 0;
}
