#include "dht_utils.h"
#include "dht_api.h"
#include "dht_hashmap.h"

#include <stdio.h>
#include <time.h>
#include <openssl/sha.h>
#include <unistd.h>
#include <uv.h>

uint64_t get_clock_diff_secs(struct timespec new_time, struct timespec old_time)
{
    uint64_t old_ms  = 0;
    uint64_t new_ms  = 0;
    uint64_t diff = 0;

    old_ms = (old_time.tv_sec * 1000) + (old_time.tv_nsec / 1000000);
    new_ms = (new_time.tv_sec * 1000) + (new_time.tv_nsec / 1000000);

    diff = ((new_ms - old_ms + 500)/1000);   /* 500 added To round off
                                                Eg: 4999 milliseconds will be 4.999 seconds
                                                So adding 500 will behave as a round() func.
                                                We are not using math.round() here because
                                                it mandates to include -lm library */

    return diff;
}

uint64_t get_clock_diff_milli_secs(struct timespec new_time, struct timespec old_time)
{
    uint64_t old_ms  = 0;
    uint64_t new_ms  = 0;
    uint64_t diff_ms = 0;

    old_ms = (old_time.tv_sec * 1000) + (old_time.tv_nsec / 1000000);
    new_ms = (new_time.tv_sec * 1000) + (new_time.tv_nsec / 1000000);

    diff_ms = new_ms - old_ms;
    return diff_ms;
}

struct timespec get_current_time()
{
    struct timespec boottime= {0};
    clockid_t clocktype = CLOCK_MONOTONIC;
#ifdef CLOCK_BOOTTIME
    clocktype = CLOCK_BOOTTIME;
#endif
    clock_gettime(clocktype, &boottime);

    return boottime;
}

uint64_t dht_get_node_key (uint16_t port, uint32_t ipv4) {
    char key_str[MAX_KEY_STR_LEN]  = {0};
    char temp_str[MAX_KEY_STR_LEN] = {0};

    inet_ntop(AF_INET, (struct in_addr *) &(ipv4), temp_str, INET_ADDRSTRLEN);
    strncpy(key_str, temp_str, MAX_KEY_STR_LEN);
    key_str[MAX_KEY_STR_LEN-1] = '\0';
    strncat(key_str, ":", (MAX_KEY_STR_LEN - strlen(key_str) - 1));
    sprintf(temp_str, "%d", port);
    strncat(key_str, temp_str, (MAX_KEY_STR_LEN - strlen(key_str) - 1));
    key_str[MAX_KEY_STR_LEN-1] = '\0';

    return (uint64_t) dht_get_hash_key((uint8_t *)key_str);
}

uint64_t dht_get_hash_key (uint8_t *key) {
    uint64_t trun[4] = {0};
    unsigned char hash[SHA256_DIGEST_LENGTH] = {0};

    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, (char *) key, strnlen((char *)key, MAX_KEY_LEN));
    SHA256_Final(hash, &sha256);

    memcpy (trun, hash, sizeof (dht_key_t));

    return (uint64_t) ((dht_key_t) (trun[0] | trun[1] | trun[2] | trun[3]));
}

int dht_sendto_thread (void *data, int type, int fd) {
    dht_notify_data_t  ndata = {0};
    int bytes_count          = 0;

    if (fd == -1) {
        printf("\n%s : Fd is invalid",__func__);
	return -1;
    }

    ndata.type = type;
    ndata.data = data;
    bytes_count = write(fd, &ndata, sizeof(dht_notify_data_t));
    if (-1 == bytes_count) {
        printf("\n%s : Failed to send data to main thread",__func__);
        return -1;
    }
    if (bytes_count != sizeof(dht_notify_data_t)) {
        printf("\n%s : Failed to send all of the data to main thread\n",__func__);
        return -1;
    }
    return 0;
}

int compare_handle_entry (void *node_data, void *data) {
    uv_poll_t *a = (uv_poll_t*) node_data;
    uv_poll_t *b = (uv_poll_t*) data;

    if (a && b) {
        if (0 == (memcmp(a, b, sizeof(uv_poll_t)))) {
            return 1;
        }
    }
    return 0;
}
