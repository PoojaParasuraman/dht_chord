#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <getopt.h>
#include <uv.h>

#include <dht_hashmap.h>
#include <dht_callbacks.h>
#include <dht_datamodel.h>
#include <dht_api.h>
#include <dht_chord.h>
#include <iniparser.h>

#define DEBUG_ENABLED 1

dht_data_t dht_data;

/**
* print debug information from the dht node
* @param node pointer to data structure containing node information
*/
static void print_node(dht_node_data_t *node) {
    char ip4_str [INET_ADDRSTRLEN];
    char ip6_str [INET6_ADDRSTRLEN];

    if (node->protocol == DHT_IPV4) {
	printf("\n Protocol		: IPV4");
	inet_ntop(AF_INET, &(node->ipv4), ip4_str, INET_ADDRSTRLEN);
	printf("\n IP Addr		: %s", ip4_str);
    }
    else if (node->protocol == DHT_IPV6) {
	printf("\n Protocol		: IPV6");
	inet_ntop(AF_INET6, &(node->ipv6), ip6_str, INET6_ADDRSTRLEN);
	printf("\n IP Addr		: %s", ip6_str);
    }
    else if (node->protocol == DHT_HOSTNAME) {
	printf("\n Protocol		: HOSTNAME");
	printf("\n Host Name		: %s", node->hostname);
    }
    else {
	printf("\n Invalid protocol");
	return;
    }
    printf("\n Port Number		: %d",node->port_number);
}
static void print_datamodel() {
    printf("\n -------------------DATAMODEL----------------------");
    printf("\n Config File 		: %s", dht_data.config_file);
    printf("\n DHT Sock File Path	: %s", dht_data.server_path);
    printf("\n API NODE		: ");
    print_node(&dht_data.api_node);
    printf("\n P2P NODE		: ");
    print_node(&dht_data.p2p_node);
    printf("\n BOOTSTRAPPER NODE	: ");
    print_node(&dht_data.bootstrapper);
}
static void dht_print_usage()
{
    printf("\n ----------------------- DHT ------------------------");
    printf("\n -c   option to provide a config file with path");
    printf("\n -p   option to povide a path for dht main thread server");
    printf("\n ----------------------------------------------------\n");
}

static inline void dht_error_argument()
{
    printf("\n Invalid Arguments");
    dht_print_usage();
}

static int dht_parse_options(int argc, char *argv[]) {
    int opt               = 0;
    int config_found      = 0;
    int server_path_found = 0;

    while ((opt = getopt (argc, argv, "c:p:"))!= -1)
    {
        switch (opt)
        {
            case 'c':
                dht_data.config_file = optarg;
		config_found         = 1;
                break;
	    case 'p':
		dht_data.server_path = optarg;
		server_path_found    = 1;
		break;
            case '?':
            default:
                dht_print_usage();
                return -1;
	}
    }
    if (!config_found || !server_path_found) {
	printf("\n Insufficient arguments\n");
	dht_print_usage();
        return -1;
    }

    return 0;
}

int dht_get_ip (char *s, dht_node_data_t *node) {
    char *token = NULL;
    if (!s) {
        printf("%s: Input string is NULL",__func__);
        return -1;
    }

    /* Ipv6 Address */
    if (s[0] == '[') {
	s = s + 1;
	token = strtok(s, "]");

        if (NULL == token) {
            printf("%s: Invalid IPv6 address and port number config",__func__);
            return -1;
        }
        if (1 != inet_pton(AF_INET6, token, &node->ipv6)) {
            printf("%s: Invalid IPv6 address",__func__);
            return -1;
        }

        token = strtok(NULL, ":");
        node->port_number = atoi(token);
	node->protocol = DHT_IPV6;
    }
    /* Host Name */
    else if (isalpha(s[0])) {

        token = strtok(s, ":");
	if (NULL == token) {
            printf("%s: Invalid IPv4 address and port number config",__func__);
            return -1;
        }
	strncpy (node->hostname, token, MAX_HOSTNAME_LEN);

        token = strtok(NULL, ":");
        node->port_number = atoi(token);
	node->protocol = DHT_HOSTNAME;
    }
    /* Ipv4 Address */
    else {
        token = strtok(s, ":");

	if (NULL == token) {
	    printf("%s: Invalid IPv4 address and port number config",__func__);
    	    return -1;
        }
        if (1 != inet_pton(AF_INET, token, &node->ipv4)) {
	    printf("%s: Invalid IPv4 address",__func__);
            return -1;
	}

        token = strtok(NULL, ":");
	node->port_number = atoi(token);
	node->protocol = DHT_IPV4;
    }

    return 0;
}

int dht_parse_config (const char *config_file) {
    dictionary *ini = NULL;
    char *s         = NULL;

    ini = iniparser_load(config_file);
    if (NULL == ini) {
        printf("%s: cannot parse file: %s", __func__, config_file);
        return -1 ;
    }

    /* Get DHT attributes */
    s = (char *) iniparser_getstring(ini, "dht:api_address", NULL);
    if (-1 == dht_get_ip(s, &dht_data.api_node)) {
        printf("%s: dht_get_ip for api node failed",__func__);
	return -1;
    }

    s = (char *) iniparser_getstring(ini, "dht:listen_address", NULL);
    if (-1 == dht_get_ip(s, &dht_data.p2p_node)) {
        printf("%s: dht_get_ip for listen node failed",__func__);
        return -1;
    }

    s = (char *) iniparser_getstring(ini, "dht:bootstrapper", NULL);
    if (-1 == dht_get_ip(s,&dht_data.bootstrapper)) {
        printf("%s: dht_get_ip for bootstrapper failed",__func__);
        return -1;
    }

    iniparser_freedict(ini);
    return 0 ;
}

int main(int argc, char *argv[]) {

    /* Parse command line options */
    if (-1 == dht_parse_options (argc, argv)) {
        printf("%s: cannot parse options", __func__);
        return -1;
    }

    /* Parse Config File */
    if (-1 == dht_parse_config(dht_data.config_file)) {
	printf("%s: cannot parse config file: %s", __func__, dht_data.config_file);
        return -1;
    }

#ifdef DEBUG_ENABLED
    print_datamodel();
#endif

    /* UV Loop init */
    dht_data.loop = (uv_loop_t *) calloc (1, sizeof(uv_loop_t));

    if (!dht_data.loop) {
        printf("%s: Unable to allocate memory for uv loop",__func__);
	return -1;
    }

    uv_loop_init(dht_data.loop);

    /* Start server thread */
    if (-1 == dht_thread_server_init(&dht_data.server_fd)) {
        printf("%s: Unable to start dht server thread",__func__);
	free(dht_data.loop);
	return -1;
    }

    dht_data.hashmap = hashmap_init();

    /* Start Poll */
    uv_poll_init   (dht_data.loop, &dht_data.server_handle, dht_data.server_fd);
    uv_poll_start  (&dht_data.server_handle, (UV_READABLE|UV_DISCONNECT), uvpoll_dht_read_cb);

    /* Start API thread */
    if (-1 == dht_init_api_thread(dht_data.api_node, dht_data.server_path)) {
        printf("%s: cannot init server for api connection", __func__);
        return -1;
    }

    if (-1 == dht_init_chord_thread(dht_data.p2p_node, dht_data.bootstrapper, dht_data.server_path)) {
        printf("%s: cannot init server for api connection", __func__);
        return -1;
    }

    /* UV Loop RUN */
    uv_run(dht_data.loop, UV_RUN_DEFAULT);

    /*UV Loop Stop*/
    uv_loop_close(dht_data.loop);
    free(dht_data.loop);
    hashmap_destroy(dht_data.hashmap);
    return 0;
}
