#include "dht_datamodel.h"
#include "dht_callbacks.h"
#include "dht_api.h"
#include "dht_hashmap.h"
#include "dht_utils.h"
#include "dht_chord.h"
#include "finger_table.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

extern dht_data_t dht_data;

int dht_process_get_msg(dht_notify_data_t *recv_msg);
int dht_process_put_request(dht_notify_data_t *recv_msg);
int dht_process_chord_put_request(dht_notify_data_t *recv_msg);
int dht_process_chord_get_request(dht_notify_data_t *recv_msg);
int dht_process_chord_get_response(dht_notify_data_t *recv_msg);
int dht_process_chord_share_data(dht_notify_data_t *recv_msg);


dht_msg_cb_obj_t dht_msg_cb_obj[] = {
    {
        .type      = DHT_GET,
        .action_cb = dht_process_get_msg,
    },
    {
        .type      = DHT_PUT,
        .action_cb = dht_process_put_request,
    },
    {
        .type      = DHT_CHORD_PUT_REQUEST,
        .action_cb = dht_process_chord_put_request,
    },
    {
        .type      = DHT_CHORD_GET_REQUEST,
        .action_cb = dht_process_chord_get_request,
    },
    {
        .type      = DHT_CHORD_GET_RESPONSE,
        .action_cb = dht_process_chord_get_response,
    },
    {
        .type      = DHT_CHORD_SHARE_DATA,
        .action_cb = dht_process_chord_share_data,
    },
};

int dht_send (int fd, void *buf, int size) {
    if (-1 != fd) {
        int len = 0;
        if (-1 == (len = send(fd, buf, size, 0))){
            printf("\n%s: send failed", __func__);
            return -1;
        }
        if (len != size) {
            printf("\n%s: Mismatch of bytes sent : %d,%d",__func__,size,len);
            return -1;
        }
        return 0;
    }
    return -1;
}

void dht_msg_data_free(dht_notify_data_t *msg) {
    if(msg && msg->data) {
        free(msg->data);
    }
    return;
}

/******************************************* Send Callbacks ****************************************/

int dht_send_put_request (api_put_t *pdata) {
    if (!pdata) {
        printf("\n%s: api_put_t data is NULL",__func__);
        return -1;
    }

    chord_put_request_t *pt_data = (chord_put_request_t *) calloc (1, sizeof(chord_put_request_t));

    if (!pt_data) {
        printf("\n%s: Calloc failed for chord_put_request_t",__func__);
        return -1;
    }

    memcpy(pt_data->key, pdata->put_data.key, MAX_KEY_LEN);
    pt_data->ttl       = pdata->ttl;
    pt_data->replica   = MAX_HOP_COUNT;
    pt_data->value_len = pdata->put_data.value_len;

    pt_data->value     = (uint8_t *) calloc (1, pdata->put_data.value_len);
    if (pt_data->value) {
        memcpy(pt_data->value, pdata->put_data.value, pdata->put_data.value_len);
    }

    dht_sendto_thread ((void *) pt_data, DHT_CHORD_PUT_REQUEST, dht_data.chord_fd);

    return 0;
}

int dht_send_get_request (uint8_t *key) {
    if (!key) {
        printf("\n%s: api_put_t data is NULL",__func__);
        return -1;
    }

    uint8_t *req_key = (uint8_t *) calloc (1, MAX_KEY_LEN);

    if (!req_key) {
        printf("\n%s: Calloc failed for req_key",__func__);
        return -1;
    }
    memcpy(req_key, key, MAX_KEY_LEN);

    dht_sendto_thread ((void *) req_key, DHT_CHORD_GET_REQUEST, dht_data.chord_fd);

    return 0;
}

int dht_send_get_response (api_put_t *ptdata, chord_node_data_t node) {

    if (!ptdata) {
	printf("\n%s: api_put_t data is NULL",__func__);
	return -1;
    }

    chord_send_get_req_data_t *get_data = (chord_send_get_req_data_t *) calloc (1, sizeof(chord_send_get_req_data_t));

    if (!get_data) {
        printf("\n%s: Calloc failed for api_put_data_t",__func__);
        return -1;
    }

    memcpy(&get_data->node, &node , sizeof(chord_node_data_t));
    memcpy(get_data->data.key, &ptdata->put_data.key, MAX_KEY_LEN);
    get_data->data.value_len = ptdata->put_data.value_len;
    get_data->data.value     = ptdata->put_data.value;

    dht_sendto_thread ((void *) get_data, DHT_CHORD_GET_RESPONSE, dht_data.chord_fd);

    return 0;
}

/******************************************* Handler Callbacks *************************************/
int dht_process_get_msg(dht_notify_data_t *recv_msg) {
    dht_hash_entry_t *entry = NULL;
    value_t value = NULL;

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    uint8_t *key = (uint8_t *) recv_msg->data;

    if (!key) {
        printf("\n%s : Key is NULL",__func__);
        return -1;
    }

    /* Get from hash */
    dht_key_t hash_key = (dht_key_t) dht_get_hash_key(key);

    if (1 == hashmap_lookup(dht_data.hashmap, hash_key, (value_t) &value)) {
        entry = (dht_hash_entry_t *) value;

        if (entry) {
            api_put_t *ptdata = (api_put_t *) entry->data;
            if (ptdata) {
                if (0 == memcmp(key, ptdata->put_data.key, MAX_KEY_LEN)) {
	    	    api_put_data_t put_data = {0};

                    memcpy(put_data.key, ptdata->put_data.key, MAX_KEY_LEN);
                    put_data.value_len = ptdata->put_data.value_len;
                    put_data.value = (uint8_t *) calloc (1, put_data.value_len);

		    if (!put_data.value) {
			return -1;
		    }
                    memcpy(put_data.value, ptdata->put_data.value, put_data.value_len);

                    if (-1 == write(dht_data.api_fd, &put_data, sizeof(api_put_data_t))) {
                         printf("\n%s: Send to api thread failed %d\n",__func__,errno);
			 free(put_data.value);
                        return -1;
	    	    }
		    return 0;
		}
	    }
	}
    }

    if (!is_local_node_successor(hash_key) && !is_only_node()) {
        /* Send Request to chord thread */
	dht_send_get_request(key);
    }

    return 0;
}

int dht_process_put_request(dht_notify_data_t *recv_msg) {
    value_t value = NULL;

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    api_put_t *data = (api_put_t *) recv_msg->data;

    if (!data) {
        printf("\n%s : value of put data is NULL",__func__);
        return -1;
    }

    dht_key_t hash_key = (dht_key_t) dht_get_hash_key(data->put_data.key);

    int status = hashmap_lookup(dht_data.hashmap, hash_key, (value_t) &value);

    dht_hash_entry_t *hentry = (dht_hash_entry_t *) value;
    if (hentry) {
        api_put_t *hpdata = (api_put_t *) hentry->data;

	if (hpdata) {
            if ((1 == status) && (0 == memcmp(hpdata->put_data.key, data->put_data.key, MAX_KEY_LEN)) &&
                (hpdata->put_data.value_len == data->put_data.value_len) && (0 == memcmp(hpdata->put_data.value, data->put_data.value, data->put_data.value_len))) {

		free(data->put_data.value);
	        return 0;
	    }
	}
    }

    if ((!is_local_node_successor(hash_key)) && (!is_only_node())) {
	/* Send Request to chord thread */
        dht_send_put_request(data);
	free(data->put_data.value);
    }
    else {
        /* Put to hash */
        dht_hash_entry_t *entry = (dht_hash_entry_t *) calloc (1, sizeof(dht_hash_entry_t));

        if (!entry) {
            printf("\n%s : Calloc failed for hash entry", __func__);
	    return -1;
        }

	api_put_t *ptdata = (api_put_t *) calloc (1, sizeof (api_put_t));

	if (!ptdata) {
	    free(entry);
            return -1;
	}

	memcpy(ptdata, data, sizeof(api_put_t));
        entry->data = ptdata;
        entry->req_time = get_current_time();

        hashmap_update(dht_data.hashmap, hash_key, (value_t) entry);
    }

    return 0;
}

int dht_process_chord_put_request(dht_notify_data_t *recv_msg) {
    value_t value     = NULL;
    api_put_t *hpdata = NULL;

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    chord_put_request_t *data = (chord_put_request_t *) recv_msg->data;

    if (!data) {
        printf("\n%s : value of put data is NULL",__func__);
        return -1;
    }

    dht_key_t hash_key = (dht_key_t) dht_get_hash_key(data->key);

    int status = hashmap_lookup(dht_data.hashmap, hash_key, (value_t) &value);
    dht_hash_entry_t *hentry = (dht_hash_entry_t *) value;

    if (hentry) {
        hpdata = (api_put_t *) hentry->data;
    }

    if (hpdata) {
        if ((1 == status) && (0 == memcmp(hpdata->put_data.key, data->key, MAX_KEY_LEN)) &&
            (hpdata->put_data.value_len == data->value_len) && (0 == memcmp(hpdata->put_data.value, data->value, data->value_len))) {

	    free(data->value);
            return 0;
	}
    }

    /* Put to hash */
    dht_hash_entry_t *entry = (dht_hash_entry_t *) calloc (1, sizeof(dht_hash_entry_t));

    if (!entry) {
        printf("\n%s : Calloc failed for hash entry", __func__);
	free(data->value);
        return -1;
    }

    api_put_t *pdata = (api_put_t *) calloc (1, sizeof(api_put_t));

    if (!pdata) {
        printf("\n%s : Calloc failed for api_put_data", __func__);
	free(data->value);
	free(entry);
        return -1;
    }

    pdata->ttl                = data->ttl;
    pdata->replication        = data->replica;
    pdata->put_data.value_len = data->value_len;
    pdata->put_data.value     = data->value;
    memcpy(&pdata->put_data.key, &data->key, MAX_KEY_LEN);

    entry->data     = pdata;
    entry->req_time = get_current_time();

    hash_key = (dht_key_t) dht_get_hash_key(pdata->put_data.key);

    hashmap_update(dht_data.hashmap, hash_key, (value_t) entry);

    return 0;
}

int dht_process_chord_get_request(dht_notify_data_t *recv_msg) {
    value_t value = NULL;
    dht_hash_entry_t *entry = NULL;

    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    chord_get_req_data_t *data = (chord_get_req_data_t *) recv_msg->data;

    if (!data) {
        printf("\n%s : value of put data is NULL",__func__);
        return -1;
    }

    /* Get from hash */
    dht_key_t hash_key = (dht_key_t) dht_get_hash_key(data->key);

    if (1 == hashmap_lookup(dht_data.hashmap, hash_key, (value_t) &value)) {
        entry = (dht_hash_entry_t *) value;

        if (entry) {
            api_put_t *ptdata = (api_put_t *) entry->data;
            if (ptdata) {
               /* Send to chord thread */
               if (0 == memcmp(&data->key, &ptdata->put_data.key, MAX_KEY_LEN)) {
                   dht_send_get_response(ptdata, data->node);
	       }
	    }
	}
    }

    return 0;
}

int dht_process_chord_get_response(dht_notify_data_t *recv_msg) {
    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    chord_get_response_t *data = (chord_get_response_t *) recv_msg->data;

    if (!data) {
        printf("\n%s : value of get response is NULL\n",__func__);
        return -1;
    }

    api_put_data_t put_data = {0};
    memcpy(put_data.key, data->key, MAX_KEY_LEN);
    put_data.value_len = data->value_len;
    put_data.value = (uint8_t *) calloc (1,data->value_len);

    if (!put_data.value) {
	free(data->value);
	return -1;
    }
    memcpy(put_data.value, data->value, data->value_len);
    free(data->value);

    if (-1 == write(dht_data.api_fd, &put_data, sizeof(api_put_data_t))) {
        printf("\n%s: Send to api thread failed %d\n",__func__,errno);
	free(put_data.value);
        return -1;
    }
    return 0;
}

void dht_check_and_share_data (dht_key_t hash_key, value_t value, dht_key_t key) {
    api_put_t *pdata = (api_put_t *) value;
    if (!pdata) {
        printf("\n%s : Hash lookup failed",__func__);
        return;
    }

    if ((hash_key < key) && is_key_beyond_succ(key)) {
        /* Send Request to chord thread */
        dht_send_put_request(pdata);
    }

    return;
}

int dht_process_chord_share_data (dht_notify_data_t *recv_msg) {
    if (!recv_msg) {
        printf("\n%s : Msg is NULL",__func__);
        return -1;
    }

    uint64_t *pkey = (uint64_t *) recv_msg->data;

    if (!pkey) {
        printf("\n%s : value of share data is NULL",__func__);
        return -1;
    }

    /* For each hash entry, check if the key belongs to pred */
    hashmap_iterator (dht_data.hashmap, dht_check_and_share_data, (dht_key_t) *pkey);
    return 0;
}

/***********************************************************************************************************/

int dht_get_msg_index(uint16_t type) {
    int index = -1;
    for (index = 0; index < ARRAY_LEN(dht_msg_cb_obj, dht_msg_cb_obj_t); index++) {
       if (dht_msg_cb_obj[index].type == type)
           break;
    }

    if ( index >= ARRAY_LEN(dht_msg_cb_obj, dht_msg_cb_obj_t) ) {
        return -1;
    }
    return index;
}

void dht_update_fd (int fd, uint16_t type) {
    int i = 0;

    switch (type) {
        case DHT_CHORD_PUT_REQUEST:
	case DHT_CHORD_GET_REQUEST:
	case DHT_CHORD_GET_RESPONSE:
	case DHT_CHORD_SHARE_DATA: {
	    dht_data.chord_fd = fd;

	    for (i = 0; i < MAX_DHT_CLIENTS; i++) {
                if (dht_data.fds[i] != fd) {
		    dht_data.api_fd = dht_data.fds[i];
		}
	    }
	    break;
	}
	case DHT_PUT:
	case DHT_GET: {
            dht_data.api_fd = fd;

	    for (i = 0; i < MAX_DHT_CLIENTS; i++) {
                if (dht_data.fds[i] != fd) {
                    dht_data.chord_fd = dht_data.fds[i];
                }
            }
            break;
        }
	default: {
	    break;
        }
    }
    return;
}

int dht_process_message (int sockfd, void *buff) {
    int status = -1;
    int index  = -1;
    dht_msg_cb_obj_t  *obj = NULL;
    dht_notify_data_t *msg = (dht_notify_data_t *) buff;

    if (!msg) {
	return -1;
    }

    do {
	dht_update_fd(sockfd, msg->type);

	index = dht_get_msg_index(msg->type);

        if (-1 == index) {
            printf("\n%s : Unknown message type %d", __func__,msg->type);
            break;
        }

        obj = &dht_msg_cb_obj[index];
        if (!obj) {
            printf("\n%s: No object for index %d", __func__, index);
            break;
        }

        if (obj->validation_cb && (0 != obj->validation_cb(msg))) {
            printf("validation failed for msg_type %d", msg->type);
            break;
        }

        if( (obj->action_cb != NULL) && (0 != obj->action_cb(msg))) {
            printf("Action cb Error for msg_type %d", msg->type);
            break;
        }
        status = 0;
    } while (0);

    dht_msg_data_free(msg);

    return status;

}

int dht_msg_handle_cb(int sockfd) {
    int msg_len            = 0;
    dht_notify_data_t buff = {0};

    if (sockfd != -1) {
        msg_len = recv(sockfd, &buff, sizeof(dht_notify_data_t), 0);

        if (-1 == msg_len) {
            printf("\n%s: read failure",__func__);
            return -1;
        }

        dht_process_message (sockfd, (void *)&buff);
    }
    return 0;
}

void uvpoll_dht_read_cb (uv_poll_t* handle, int status, int events)
{
    int fd = -1;
    int i  = 0;
    if (handle) {
        uv_fileno((uv_handle_t *)handle, &fd);
        if((status < 0) || (events & UV_DISCONNECT))
        {
            uv_poll_stop(handle);
            if (events & UV_DISCONNECT) {
                if (fd == dht_data.server_fd) {
                    close(dht_data.server_fd);
                    int ret = dht_thread_server_init(&dht_data.server_fd);
                    while (ret < 0) {
                        ret = dht_thread_server_init(&dht_data.server_fd);
                    }
                    uv_poll_init  (dht_data.loop, &dht_data.server_handle, dht_data.server_fd);
                    uv_poll_start (&dht_data.server_handle, (UV_READABLE|UV_DISCONNECT), uvpoll_dht_read_cb);
                }
                else {
	            for (i = 0; i < MAX_DHT_CLIENTS; i++) {
	                if (dht_data.fds[i] == fd) {
			    dht_data.fds[i] = -1;
			    if (fd == dht_data.api_fd)
				dht_data.api_fd = -1;
			    else
				dht_data.chord_fd = -1;
			    break;
			}
		    }

		    if (dht_data.fds[0] == -1) 
	                dht_data.fds[0] = dht_data.fds[1];

                    dht_data.client_count--;
		    close(fd);
                }
            }
        }
        else if (events & UV_READABLE) {
            if (fd == dht_data.server_fd) {
		 if (dht_data.client_count >= MAX_DHT_CLIENTS) {
	             printf("\n too many connections\n");
		     return;
		 }
                 /* Accept connections from client */
                 int client_fd = -1;
                 if (-1 == (client_fd = accept(dht_data.server_fd, NULL,NULL))) {
                     printf("\n%s: accept client connection failed",__func__);
                     return;
                 }
	         dht_data.fds[dht_data.client_count] = client_fd;
                /* Start uv poll for the client */
                uv_poll_init  (dht_data.loop, &dht_data.client_handles[dht_data.client_count], client_fd);
                uv_poll_start (&dht_data.client_handles[dht_data.client_count], (UV_READABLE|UV_DISCONNECT), uvpoll_dht_read_cb);
                dht_data.client_count++;
            }
            else {
                 dht_msg_handle_cb(fd);
	    }
        }
    }

    return;
}

int dht_thread_server_init (int *sockfd) {
    int server_fd             = -1;
    struct sockaddr_un server = {0};

    if (-1 == (server_fd = socket(AF_UNIX, SOCK_SEQPACKET, 0))) {
        printf("\n%s: Socket creation failed %d", __func__,errno);
        return -1;
    }
    memset(&server, 0, sizeof(struct sockaddr_in));

    server.sun_family      = AF_UNIX;
    memset(server.sun_path, 0, MAX_SOCK_PATH_LEN);
    strncpy(server.sun_path+1, dht_data.server_path, MAX_SOCK_PATH_LEN);

    if (0 != bind(server_fd, (struct sockaddr *)&server, sizeof(struct sockaddr_un))) {
        printf("\n%s: Socket bind failed %d\n",__func__,errno);
        return -1;
    }

    if (0 != listen(server_fd, MAX_DHT_CLIENTS)) {
        printf("\n%s: Listen failed",__func__);
        return -1;
    }

    *sockfd = server_fd;
    return 0;
}

